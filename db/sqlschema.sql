BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "Coin" (
	"id"	INTEGER NOT NULL UNIQUE,
	"name"	TEXT,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "CoinRate" (
	"id"	INTEGER NOT NULL UNIQUE,
	"idcoin"	INTEGER,
	"rate"	REAL,
	"date"	INTEGER,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "Tag2Todo" (
	"id"	BLOB,
	"idtag"	BLOB,
	"idtodo"	BLOB,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "Tag" (
	"id"	BLOB,
	"parentid"	BLOB,
	"priority"	BLOB,
	"color"	TEXT,
	"name"	TEXT,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "User" (
	"id"	BLOB NOT NULL UNIQUE,
	"name"	TEXT NOT NULL UNIQUE,
	"idi"	INTEGER,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "CoinInvestmentGroup" (
	"id"	INTEGER NOT NULL UNIQUE,
	"iduser"	BLOB,
	"name"	TEXT NOT NULL,
	"datecreated"	INTEGER,
	"lastused"	INTEGER,
	"uid"	BLOB,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "CoinInvestment" (
	"id"	INTEGER NOT NULL UNIQUE,
	"iiduser"	INTEGER,
	"idcoin"	INTEGER NOT NULL,
	"idgroup"	INTEGER,
	"amount"	REAL,
	"valueinitial"	REAL,
	"valueinitialEUR"	REAL,
	"iduser"	BLOB,
	PRIMARY KEY("id" AUTOINCREMENT)
);
CREATE TABLE IF NOT EXISTS "Note" (
	"id"	BLOB,
	"idint"	INTEGER,
	"idfk"	BLOB,
	"idtodo"	INTEGER,
	"idjournal"	INTEGER,
	"idkb"	INTEGER,
	"idtask"	INTEGER,
	"type"	INTEGER,
	"status"	INTEGER,
	"note"	TEXT,
	"notehtml"	TEXT,
	"idusercreated"	BLOB,
	"datecreated"	INTEGER,
	"idusermodified"	BLOB,
	"datemodified"	INTEGER,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "TaskGroup" (
	"id"	BLOB NOT NULL UNIQUE,
	"idint"	INTEGER,
	"status"	INTEGER,
	"priority"	INTEGER,
	"name"	TEXT,
	"idusercreated"	BLOB,
	"datecreated"	INTEGER,
	"idusermodified"	BLOB,
	"datemodified"	INTEGER
);
CREATE TABLE IF NOT EXISTS "Todo" (
	"id"	BLOB NOT NULL UNIQUE,
	"idint"	INTEGER,
	"iduser"	BLOB,
	"idnote"	INTEGER,
	"type"	INTEGER,
	"group"	BLOB,
	"done"	INTEGER,
	"priority"	INTEGER,
	"status"	INTEGER,
	"name"	TEXT,
	"url"	INTEGER,
	"notes"	TEXT,
	"datedue"	INTEGER,
	"idusercreated"	BLOB,
	"datecreated"	INTEGER,
	"idusermodified"	BLOB,
	"datemodified"	INTEGER,
	PRIMARY KEY("id")
);
CREATE TABLE IF NOT EXISTS "KBSync" (
	"id"	BLOB,
	"idparent"	BLOB,
	"idkb"	BLOB,
	"type"	INTEGER,
	"md5"	TEXT,
	"name"	TEXT,
	"tagname"	TEXT,
	"path"	TEXT,
	"error"	TEXT,
	"datesynced"	INTEGER,
	PRIMARY KEY("id")
);
COMMIT;
