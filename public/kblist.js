var viewModelTodoList = {};
var viewModelTagList = {};
var viewModelDueList = {};

var createViewModelDueList = function () {
    var self = this;

    self.items = ko.observableArray([]);
    self.filteredDueTodayItems = ko.pureComputed(function() {
        var ret = [];
        
        
        if (self.items() && self.items().length > 0) {
            var dateTodayStr = utils.getDateStrFromMS(new Date().getTime());

            for (var i = 0; i < self.items().length; i++) {
                var item = self.items()[i];
                if (item().id == -1) {
                    var newItem = self.items.splice(i, 1)[0];
                    self.items.unshift(newItem);
                    i = 0;
                    continue;
                }

                if (item().datedue && utils.getDateStrFromMS(item().datedue) == dateTodayStr) {
                    ret.push(item);
                }
            }
        }

        return ret;
    }, self);

    self.filteredDueSoonItems = ko.pureComputed(function() {
        var ret = [];
        
        var dateNow = new Date().getTime();

        if (self.items() && self.items().length > 0) {
            for (var i = 0; i < self.items().length; i++) {
                var item = self.items()[i];
                if (item().id == -1) {
                    var newItem = self.items.splice(i, 1)[0];
                    self.items.unshift(newItem);
                    i = 0;
                    continue;
                }

                if (item().datedue && item().datedue > dateNow) {
                    ret.push(item);
                }
            }
        }

        return ret;
    }, self);

    self.filteredPastDueItems = ko.pureComputed(function() {
        var ret = [];
        
        var dateNow = new Date().getTime();
        var dateTodayStr = utils.getDateStrFromMS(dateNow);

        if (self.items() && self.items().length > 0) {
            for (var i = 0; i < self.items().length; i++) {
                var item = self.items()[i];
                if (item().id == -1) {
                    var newItem = self.items.splice(i, 1)[0];
                    self.items.unshift(newItem);
                    i = 0;
                    continue;
                }

                if (item().datedue && item().datedue < dateNow) {
                    if (utils.getDateStrFromMS(item().datedue) != dateTodayStr)
                    ret.push(item);
                }
            }
        }

        return ret;
    }, self);

}
var createViewModelTodoList = function () {
    var self = this;

    self.userId = ko.observable(-1);
    self.userName = ko.observable("");
    self.userList = ko.observable([]);
    self.searchTagUrl = ko.observable(""); 
    self.searchTagUrlUsed = ko.observable(false);
    self.searchShowDone = ko.observable(false);
    self.searchShowDel = ko.observable(false);
    self.showInp = ko.observable(true);
    self.inpCapture = ko.observable(null);
    
    self.items = ko.observableArray([]);
    self.tagItems = ko.observableArray([]);
    self.tag2TodoItems = ko.observableArray([]);
    self.addTodoItemName = ko.observable("");
    self.addExcludeTag = ko.observable("");
    self.searchTagItems = ko.observableArray([]);
    self.searchTagExcludeItems = ko.observableArray([]);
    self.searchTerm = ko.observable("");
    self.itemsModified = ko.observable(false);
    self.filteredItems = ko.pureComputed(function() {
        //var ret =ko.observableArray([]);
        var ret = [];
        if (self.itemsModified()) {
            self.itemsModified(false);
        }
        /*if (self.searchTerm()) {
            for (var i = 0; i < self.items().length; i++) {
                var item = self.items()[i];
                if (item().name.indexOf(viewModelTodoList.searchTerm()) != -1) {
                    ret.push(item);
                }
            }
        } else */
        {
            if (self.items() && self.items().length > 0) {
                //ret(self.items());
                var index = -1;

                for (var i = 0; i < self.items().length; i++) {
                    var item = self.items()[i];
                    if (item().id == -1) {
                        index = i;
                    }
                }

                if (index != -1) {
                    var newItem = self.items.splice(index, 1)[0];
                    self.items.unshift(newItem);
                }

                ret = self.items();
            }
        }

        return ret;
    }, self);


    self.refresh = function(data, event) {
        getTodoItemList();
        return true;
    };

    self.delTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiDelTodoItem', ko.toJSON(req), false, (json) => {
            getTodoItemList();
        });
    };

    self.disableTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiDisableTodoItem', ko.toJSON(req), false, (json) => {
            getTodoItemList();
        });
    };

    self.archiveTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiArchiveTodoItem', ko.toJSON(req), false, (json) => {
            getTodoItemList();
        });
    };
    

    self.removeSearchTag = function(data, event) {
        if (!(self.searchTagUrlUsed() && self.searchTagItems.length == 1)) {
            var tagIndex = getIndexById(self.searchTagItems, data.id);
            self.searchTagItems.splice(tagIndex, 1);
            getTodoItemList();
        }  
    };

    self.removeSearchTagExclude = function(data, event) {
        if (!(self.searchTagUrlUsed() && self.searchTagExcludeItems.length == 1)) {
            var tagIndex = getIndexById(self.searchTagExcludeItems, data.id);
            self.searchTagExcludeItems.splice(tagIndex, 1);
            getTodoItemList();
        }  
    };

    self.searchForTerm = function(data, event) {
        getTodoItemList();
    };

    self.removeTagRelation = function(data, event) {
        //viewModelTag.tagId = data;
    
        xhrPost('/todo/apiDelTagRelation', ko.toJSON(viewModelTagList));
    };

    self.saveTodoItem = function(data, event) {
        var req = {
            id : data.id,
            userid: self.userId,
            idnote: data.idnote,
            type: 2,
            name: data.name,
            notes: data.notes,
            notehtml: data.notehtml,
            done : data.done,
            priority: data.priority,
            status: data.status,
            url: data.url,
            duedate: getDueDateVal(data)

        }
        
        xhrPost('/todo/apiUpd', ko.toJSON(req), false, (json) => {
            self.toggleEdit(data);
            viewModelTodoList.addTodoItemName("");
            getTodoItemList();
        });

        return true;
    }

    self.toggle = function(item) {
        if (item.showDetails()) {
            item.showDetails(false);
        } else {
            item.showDetails(true);
            acForId(item);
        }
      /* if (self.showInp()) {
            self.showInp(false);
        } else {
            self.showInp(true);
        }*/
    };

    self.toggleEdit = function(item) {
        if (item.showEdit()) {
            item.showEdit(false);
        } else {
            item.showEdit(true);
        }
    }

    self.toggleShowFullNotes = function(item) {
        if (item.showFullNotes()) {
            item.showFullNotes(false);
            item.notesDisplay(getTodoNotesLine(item.notes, false));
        } else {
            item.showFullNotes(true);
            item.notesDisplay(getTodoNotesLine(item.notes, true));
        }
    }

    self.addTagItemName = ko.observable("");
    
    self.delTagItem = function(data, event) {
        var req = {
            tagId: data.id
        }

        xhrPost('/todo/apiDelTag', ko.toJSON(req), false, (json) => {
            getTagList();
        });
    };
 };

var createViewModelTagList = function () {
    var self = this;

    self.items = ko.observableArray([]);
    self.addTagItemName = ko.observable("");
    
    self.delTagItem = function(data, event) {
        var req = {
            tagId: data.id
        }

        xhrPost('/todo/apiDelTag', ko.toJSON(req), false, (json) => {
            getTagList();
        });
    };
}

function clearInput(id) {
    document.getElementById(id).value = ""; 
}

self.createItem = function(name, note, noteHtml, showDetails, showEdit) {
    var item = {
        id : -1,
        idint : 0,
        name: name,
        notes: note,
        notehtml: noteHtml,
        done : "",
        priority: "",
        url: "",
        showDetails: ko.observable(showDetails),
        showEdit: ko.observable(showEdit),
        showFullNotes : ko.observable(false),
        notesDisplay: ko.observable(""),
        datedueDisplay: ko.observable(""),
        tags: [],
        idusercreated: -1,
        datecreated : 0,
        idusermodified : -1,
        datemodified: 0
    }

    return item;
}

function addTodoItem() {
    

    getTitleFromUrl(viewModelTodoList.addTodoItemName, (htmlTitle) => {
        var title = viewModelTodoList.addTodoItemName; 
        var url = "";

        if (htmlTitle && htmlTitle.length > 0) {
            title = htmlTitle;
            url = viewModelTodoList.addTodoItemName; 
        }

        var item = createItem(title, "", "", true, true);
        item.url = url;
        viewModelTodoList.items().push(new ko.observable(item));
        viewModelTodoList.searchTerm("");
        document.getElementById("auto-1").value = "";
    });
    
    
    
}
function checkDeltaForFormatting(delta) {
    var ret = false;

    for (var i = 0; i < delta.ops.length; i++) {
        var currentChange = delta.ops[i];
        if (currentChange.attributes) {
            ret = true;
        }
    }

    return ret;
}

function addTodoItemNote() {
    var note = "";
    var noteHtml = "";
    var delta = viewModelTodoList.inpCapture().getContents(); 
    note = viewModelTodoList.inpCapture().getText();
    if (checkDeltaForFormatting(delta)) {
        noteHtml = viewModelTodoList.inpCapture().getHtml();
        //note = viewModelTodoList.inpCapture().getHtml();
    } else {
        note = viewModelTodoList.inpCapture().getText();
    }
        
    var item = createItem("", note, noteHtml, true, true);
    viewModelTodoList.items().push(new ko.observable(item));
    viewModelTodoList.itemsModified(true);
    //viewModelTodoList.searchTerm("");
    
}

function setSearchTerm(data) {
    viewModelTodoList.addTodoItemName(data.name);
    viewModelTodoList.searchTerm(data.name);
    setTimeout(() => {
        data.showDetails(true);
    }, 100);
    
}

function addTodoItemold() {
    var req = {
        name : viewModelTodoList.addTodoItemName
    };

    xhrPost('/todo/apiUpd', ko.toJSON(req), false, (json) => {
        viewModelTodoList.addTodoItemName("");
        //clearInput('inpAddTodoItem');
        //getTodoItemList();
        refresh();
    });
}
/*
function addTagItem() {
    var req = {
        name : viewModelTodoList.addTagItemName
    };

    xhrPost('/todo/apiAddTag', ko.toJSON(req), false, (json) => {
        
        viewModelTodoList.addTagItemName("");
        getTagList();
    });
}
*/

function addTagRelationSearch(data) {
    addTagRelation(data, () => {
        viewModelTodoList.toggleEdit(data);
        viewModelTodoList.addTagItemName("");

        init();
    });
}

function addTagRelationEdit(data) {
    addTagRelation(data, () => {
        var elementId = "auto" + data.id;
        document.getElementById(elementId).value = ""; 
        refresh();
        data.showDetails(false);
    });
}

function addTagRelation(data, cb) {
    var req = {
        todoItemId : data.id
    }

    if (data.selectedTag) {
        req.tagId = data.selectedTag.id;
    } else {
        req.tagName = viewModelTodoList.addTagItemName
    }

    xhrPost('/todo/apiAddTagRelation', ko.toJSON(req), false, (json) => {
        if (cb) {
            cb();
        }
    });
}


function refresh() {
    var userId = viewModelTodoList.userId();

    getTagList(userId, (tags) => {
        getTag2TodoList(userId, (tag2Todos) => {
            getTodoItemList(userId, (tag2Todos) => {
            });        
        });
    });
}

function init(userId, userName) {
    viewModelTodoList = new createViewModelTodoList();
    viewModelDueList = new createViewModelDueList();
    viewModelTagList = new createViewModelTagList();
    ko.applyBindings(viewModelTodoList, document.getElementById("tdTodoListId"));
    //ko.applyBindings(viewModelDueList, document.getElementById("tdDueListId"));
    ko.applyBindings(viewModelTagList, document.getElementById("tdTagListId"));

    getUserList();
    
    /*
    var searchTagName = '#{tags}';
    if (searchTagName) {
        var searchTag = getTagByName(searchTagName);
        viewModelTodoList.searchTagItems.push(searchTag);
    }*/

    if (userId && viewModelTodoList.userId() == -1) {
        viewModelTodoList.userId(userId);
    }
    
    if (userName) {
        viewModelTodoList.userName(userName);
    }

    refresh();

    Quill.prototype.getHtml = function() {
        return this.container.querySelector('.ql-editor').innerHTML;
    };
    viewModelTodoList.inpCapture(new Quill('#bubble-container', {
        placeholder: 'Enter a some knowledge ...',
        theme: 'snow' 
      }));

    /*
    var searchTagName = '#{tags}';
    if (searchTagName) {
      viewModelTodoList.searchTagUrl(searchTagName);
    }
*/
    var sel = "#inpAddTodoItem";
    new autoCompleteForId(sel, 
    (term) => {
        var tag = getTagByName(term);
        viewModelTodoList.searchTagItems.push(tag);
        document.getElementById("inpAddTodoItem").value = ""; 
        getTodoItemList();
    },
    (searchTerm) => {
        if (searchTerm && searchTerm.length > 0) {
            console.log(searchTerm +":"+ searchTerm.length)
            if (searchTerm[searchTerm.length-1] == ' ') {
                viewModelTodoList.searchTerm(searchTerm);
                getTodoItemList();
            }
        }
        
    });

    var sel = "#inpSearchNot";
    new autoCompleteForId(sel, 
    (term) => {
        var tag = getTagByName(term);
        viewModelTodoList.searchTagExcludeItems.push(tag);
        document.getElementById("inpSearchNot").value = ""; 
        getTodoItemList();
    });

}

function autoCompleteForId(sel, onSelect, onSuggestNone) {
    //var sel = "#auto" + item.id;

    return autoComplete({
        selector: sel,
        minChars: 0,
        onSelect: function(e, term, element){
            if (onSelect) {
                onSelect(term);
            }
        }, 
        onSuggestNone:  function(searchTerm) {
            if (onSuggestNone) {
                onSuggestNone(searchTerm);
            }
          
        },
        source: function(term, suggest){
            term = term.toLowerCase();
            var choices = getTagNames(); 
            var matches = [];
            for (i=0; i<choices.length; i++) {
              var item = choices[i].toLowerCase();
  
              var index = item.indexOf(term);
              if (index != -1) {
                  matches.push(choices[i]);
                } else {
                  if (term ==' ') {
                    matches.push(choices[i]);
                  }
                }
            }
            suggest(matches, term);
        }
    });
}

function acForId(item) {
    var itemId = item.id;
    var sel = "#auto" + item.id;

    item.ac =  autoCompleteForId(sel, (term) => {
        var tag = getTagByName(term);
        var myItem = utils.getById(viewModelTodoList.items(), itemId);
        myItem.selectedTag = tag;
    });
}



function getTagNames() {
    var ret = [];

    for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
        var element = viewModelTodoList.tagItems()[i];
        ret.push(element.name);
    }

    return ret;
}

function getTagIds(tagItems) {
    var ret = [];

    for (var i = 0; i < tagItems.length; i++) {
        var element = tagItems()[i];
        if (element) {
            ret.push(element.id);
        }
    }

    return ret;
}

function getTagByName(name) {
    var ret = null;

    for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
        var element = viewModelTodoList.tagItems()[i];
        if (element.name == name) {
            ret = element;
             
            break;
        }
    }

    return ret;
}

function getTagsForTodoItemId(id) {
    var ret = [];

    if (id != -1) {
        viewModelTodoList.tag2TodoItems().forEach(element => {
            if (element.idtodo == id) {
                var tag = getById(viewModelTodoList.tagItems(), element.idtag);
                ret.push(tag);
            }
        });
    }
    
    return ret;
}



function getTodoItemList(userId) {
    var apiMethod = '/todo/apiGetTodoItemList';
    
    var req = {
        userId : viewModelTodoList.userId,
        type : utils.TODOTYPEDATA.KB.ID,
        searchTerm: viewModelTodoList.searchTerm,
        tagBase:  "",
        tagSearch:  "",
        tagIds : null,
        tagIdsExclude : null,
        showDone: viewModelTodoList.searchShowDone,
        showDel: viewModelTodoList.searchShowDel
    };

    if (viewModelTodoList.searchTagItems.length > 0) {
        req.tagIds = getTagIds(viewModelTodoList.searchTagItems);
        apiMethod = '/todo/apiGetTodoItemListByTags';
    }

    if (viewModelTodoList.searchTagExcludeItems.length > 0) {
        req.tagIdsExclude = getTagIds(viewModelTodoList.searchTagExcludeItems);
        apiMethod = '/todo/apiGetTodoItemListByTags';
    }


    xhrPost(apiMethod, ko.toJSON(req), true, (json) => {
        for (var i =0 ; i < json.length; i++) {
            json[i].showDetails = ko.observable(false);
            json[i].showEdit = ko.observable(false);
            json[i].showFullNotes = ko.observable(false);
            json[i].notesDisplay = ko.observable(getTodoNotesLine(json[i].notes, false));
            json[i].tags = getTagsForTodoItemId(json[i].id);
            json[i].datedueDisplay = utils.getDateStrFromMS(json[i].datedue);
            json[i].datedue = json[i].datedue;
            json[i].datePicker = null;
            
            json[i] = ko.observable(json[i]);
        }
        /*json.forEach(element => {
            element.showDetails = false;

        });*/

        viewModelTodoList.items(json);
        viewModelDueList.items(json);
    });
}

function getTagList(userId, cb) {
    viewModelTagList.todoItemId = -1;

    xhrPost('/todo/apiGetTagList', ko.toJSON(viewModelTagList), true, (json) => {
        viewModelTodoList.tagItems(json);
        if (viewModelTodoList.searchTagUrl()) {
            viewModelTodoList.searchTagUrlUsed(true);
            var searchTag = getTagByName(viewModelTodoList.searchTagUrl());
            viewModelTodoList.searchTagItems.push(searchTag);
            
            viewModelTodoList.searchTagUrl("");
        }

        if (cb) {
            cb(viewModelTodoList.tagItems);
        }
    });
}

function getUserList(cb) {
    xhrPost('/todo/apiGetUserList', null, true, (json) => {
        viewModelTodoList.userList(json);
        if (cb) {
            cb(viewModelTodoList.tagItems);
        }
    });
}



function getTag2TodoList(userId, cb) {
    viewModelTagList.todoItemId = -1;

    xhrPost('/todo/apiGetTag2TodoList', ko.toJSON(viewModelTagList), true, (json) => {
        viewModelTodoList.tag2TodoItems(json);
        if (cb) {
            cb(viewModelTodoList.tag2TodoItems);
        }
    });
}

function getTitleFromUrl(url, cb) {
    var myUrl = url;
    if (typeof url =='function') {
        myUrl = url();
    }

    if (!utils.isUrl(myUrl)) {
        cb("");
        return;
    }

    var req = {
        url: url
    };

    xhrPost('/todo/apiGetTitleFromUrl', ko.toJSON(req), true, (json) => {
        if (cb) {
            cb(json);
        }
    });    
}

var TODOSTATUS = {
	ACTIVE : 0,
	INACTIVE : 1,
	ARCHIVED : 2
};

function getTodoStatusText(status) {
    var ret = "";
    
    if (status == TODOSTATUS.INACTIVE) {
        ret = "inactive";
    } else if (status == TODOSTATUS.ARCHIVED) {
        ret = "archived";
    }

    return ret;
}

function getTodoStatusColor(status, done) {
    var ret = "colorItemActive";

    if (done) {
        ret = "colorItemDone";
    } else {
        if (status == TODOSTATUS.INACTIVE) {
            ret = "colorItemInActive";
        } else if (status == TODOSTATUS.ARCHIVED) {
            ret = "colorItemArchived";
        }
    }

    return ret;
}

function getTodoPriorityText(prio) {
    var ret = prio;
    
    if (prio<= 0) {
        ret = "";
    } else if (prio  == "null") {
        ret = "";
    }

    return ret;
}

function getTodoNotesLine(notes, showAll) {
    var maxLen = 50;
    var ret = "";

    if (showAll) {
        return getTodoNotes(notes);
    } else {
        if (notes) {
            var sp = notes.split('\n');
            if (sp.length > 0) {
                ret = sp[0]; 
                if (ret.length > maxLen) {
                    ret = ret.substr(0, maxLen) + "...";
                }
            }
        }
    }
    
    return ret;
}



function getTodoEditUrl(id) {
    return '/todo/edit/' + id;
}


/*
function getTodoSearchUrl(tag) {
    return '/todo?searchTag=' + tag;
}*/

function getTodoSearchUrl(tag) {
    //var tag = item.name;
    return '/todo/' + tag;
}

function getUserName(userId) {
    var ret = "";

    var user = getById(viewModelTodoList.userList(), userId);
    if (user && user.id) {
        ret = user.name;
    }

    return ret;
}
function getCreatedModifiedStr(userIdCreated, dateCreated, userIdModified, dateModified) {
    var ret = ""; 
    var u = getById(viewModelTodoList.userList(), userIdCreated);
    if (userIdCreated) {
        ret = `Created by ${getUserName(userIdCreated)} at ${utils.getDateTimeStrFromMS(dateCreated)}`;
        
        if (!(userIdModified == userIdModified && dateCreated == dateModified)) {
            ret += `, modified by ${getUserName(userIdModified)} at ${utils.getDateTimeStrFromMS(dateModified)}`;
        }
    }

    return ret;
}

function showDatePicker(data, ev) {
    if (!data.datePicker) {
        data.datePicker = setDatePicker(data, ev.target);
        
    } 
    
    if (data.datePicker) {
        data.datePicker.show();
    }
}

function getDueDateVal(data) {
    var ret = data.datedue;

    if (data.datePicker) {
        ret = utils.getMSFromDateStr(data.datePicker.element.value);
    } 
    
    return ret;
}

function setDatePicker(data, elem) {
    return new Datepicker(elem, {
        // ...options
        format: "dd.mm.yyyy",
        weekStart: 1,
        //showOnClick: true,
        todayBtn: true,
        todayBtnMode: 1,
        todayHighlight: true,
        //autohide: true,
        changeDate: (d) => {
            var i = 0;
        }
    });
}