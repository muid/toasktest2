var viewModelTodoItem = {};
var viewModelTag = {};

var createViewModelTodoItem = function (id, userid, name, url, priority, done, notes) {
    var self = this;
  
    self.id = ko.observable(id);
    self.items = ko.observableArray([]);
    //self.kbitem = ko.observable({});
    self.userid = ko.observable(userid); 
    self.type = ko.observable(4);
    self.status = ko.observable(0);
    self.name = ko.observable(name);
    self.url = ko.observable(url);
    self.priority = ko.observable(priority);
    self.done = ko.observable(done);
    self.notes = ko.observable(notes);
};

var createViewModelTagRelation = function (tagId, idTodoItem) {
    var self = this;
  
    self.tagId = ko.observable(tagId);
    self.todoItemId = ko.observable(idTodoItem);
    self.tagName = ko.observable("");
}

function init(userId, userName, kbId) {
                //data from to another place in the load
    viewModelTodoItem = new createViewModelTodoItem('!{todoitem.id}','!{user.id}', '!{todoitem.name}', '!{todoitem.url}', '!{todoitem.priority}', '!{todoitem.done}', '!{todoitem.notes}');
    viewModelTag = new createViewModelTagRelation(0, '!{todoitem.id}');
    ko.applyBindings(viewModelTodoItem, document.getElementById("myBody"));
    ko.applyBindings(viewModelTag, document.getElementById("tagBody"));

    getKbItem(kbId, (kbItem) => {
        kbItem['showDetails'] =ko.observable(false);
        kbItem['showEdit'] = ko.observable(false),
        //kbItem['showDetails'] =false;
        //kbItem['notesDisplay'] = ko.observable(""),
        kbItem['notesDisplay'] = ko.observable(getTodoNotes(kbItem, false));

        kbItem['tags'] = [],
        viewModelTodoItem.items.push(kbItem);
    });
}

function getKbItem(kbId, cb) {
    var req = {
        itemId: kbId
    };

    xhrPost('/todo/apiGetTodoItemById', ko.toJSON(req), true, (json) => {
        if (cb) {
            cb(json);
        }
    });    
}

function saveTodoItem() {
    xhrPost('/todo/apiUpd', ko.toJSON(viewModelTodoItem));
}

function addTagRelation() {
    xhrPost('/todo/apiAddTagRelation', ko.toJSON(viewModelTag));
}

function delTagRelation(tagId) {
    viewModelTag.tagId = tagId;

    xhrPost('/todo/apiDelTagRelation', ko.toJSON(viewModelTag));
}

