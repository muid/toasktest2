
var viewModelCoinList = {};
var viewModelTagList = {};

var createViewModelCoinInvestmentList = function (selectedGroup) {
    var self = this;

    self.userId = ko.observable();
    self.investmentGroupSelected = ko.observable({ name: "Current", id: selectedGroup });
    self.investmentGroups = ko.observableArray([]);
    self.investmentItems = ko.observableArray([]);
 
    self.onGroupSelect = function(data) {
        var i = data;
        refresh(self.investmentGroupSelected().id, true);
        //alert("sdf")
    }
    self.filteredInvestmentItems  = ko.pureComputed(function() {
        //var ret =ko.observableArray([]);
        var ret = [];
        ret = self.investmentItems();
        /*if (self.searchTerm()) {
            for (var i = 0; i < self.items().length; i++) {
                var item = self.items()[i];
                if (item().name.indexOf(viewModelTodoList.searchTerm()) != -1) {
                    ret.push(item);
                }
            }
            //ret([]);
            //ret = [];
        } else {
            if (self.items() && self.items().length > 0) {
                //ret(self.items());
                var index = -1;

                for (var i = 0; i < self.items().length; i++) {
                    var item = self.items()[i];
                    if (item().id == -1) {
                        index = i;
                    }
                }

                if (index != -1) {
                    var newItem = self.items.splice(index, 1)[0];
                    self.items.unshift(newItem);
                }

                ret = self.items();
            }
        }*/

        return ret;
    }, self);

}

var createViewModelTodoList = function () {
    var self = this;

    self.searchTagUrl = ko.observable(""); 
    self.searchTagUrlUsed = ko.observable(false);
    self.searchShowDone = ko.observable(false);
    self.searchShowDel = ko.observable(false);
    self.showInp = ko.observable(true);
    
    self.items = ko.observableArray([]);
    self.tagItems = ko.observableArray([]);
    self.tag2TodoItems = ko.observableArray([]);
    self.addTodoItemName = ko.observable("");
    self.searchTagItems = ko.observableArray([]);
    self.searchTerm = ko.observable("");
    self.filteredItems = ko.pureComputed(function() {
        //var ret =ko.observableArray([]);
        var ret = [];
        if (self.searchTerm()) {
            for (var i = 0; i < self.items().length; i++) {
                var item = self.items()[i];
                if (item().name.indexOf(viewModelTodoList.searchTerm()) != -1) {
                    ret.push(item);
                }
            }
            //ret([]);
            //ret = [];
        } else {
            if (self.items() && self.items().length > 0) {
                //ret(self.items());
                var index = -1;

                for (var i = 0; i < self.items().length; i++) {
                    var item = self.items()[i];
                    if (item().id == -1) {
                        index = i;
                    }
                }

                if (index != -1) {
                    var newItem = self.items.splice(index, 1)[0];
                    self.items.unshift(newItem);
                }

                ret = self.items();
            }
        }

        return ret;
    }, self);

/*
    self.refresh = function(data, event) {
        getTodoItemList();
        return true;
    };*/

    self.delTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiDelTodoItem', ko.toJSON(req), false, (json) => {
            getTodoItemList();
        });
    };

    self.disableTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiDisableTodoItem', ko.toJSON(req), false, (json) => {
            getTodoItemList();
        });
    };

    self.archiveTodoItem = function(data, event) {
        var req = {
            id: data.id
        }

        xhrPost('/todo/apiArchiveTodoItem', ko.toJSON(req), false, (json) => {
            getTodoItemList();
        });
    };
    

    self.removeSearchTag = function(data, event) {
        if (!(self.searchTagUrlUsed() && self.searchTagItems.length == 1)) {
            var tagIndex = getIndexById(self.searchTagItems, data.id);
            self.searchTagItems.splice(tagIndex, 1);
            getTodoItemList();
        }  
    };

    self.removeTagRelation = function(data, event) {
        //viewModelTag.tagId = data;
    
        xhrPost('/todo/apiDelTagRelation', ko.toJSON(viewModelTagList));
    };

    self.saveTodoItem = function(data, event) {
        var req = {
            id : data.id,
            name: data.name,
            notes: data.notes,
            done : data.done,
            priority: data.priority,
            url: data.url

        }
        
        xhrPost('/todo/apiUpd', ko.toJSON(req), false, (json) => {
            self.toggleEdit(data);
            viewModelTodoList.addTodoItemName("");
            getTodoItemList();
        });

        return true;
    }

    self.toggle = function(item) {
        if (item.showDetails()) {
            item.showDetails(false);
        } else {
            item.showDetails(true);
        }
      /* if (self.showInp()) {
            self.showInp(false);
        } else {
            self.showInp(true);
        }*/
    };

    self.toggleEdit = function(item) {
        if (item.showEdit()) {
            item.showEdit(false);
        } else {
            item.showEdit(true);
        }
    }

    self.toggleShowFullNotes = function(item) {
        if (item.showFullNotes()) {
            item.showFullNotes(false);
            item.notesDisplay(getTodoNotesLine(item.notes, false));
        } else {
            item.showFullNotes(true);
            item.notesDisplay(getTodoNotesLine(item.notes, true));
        }
    }

    self.addTagItemName = ko.observable("");
    
    self.delTagItem = function(data, event) {
        var req = {
            tagId: data.id
        }

        xhrPost('/todo/apiDelTag', ko.toJSON(req), false, (json) => {
            getTagList();
        });
    };
 };

var createViewModelTagList = function () {
    var self = this;

    self.items = ko.observableArray([]);
    self.addTagItemName = ko.observable("");
    
    self.delTagItem = function(data, event) {
        var req = {
            tagId: data.id
        }

        xhrPost('/todo/apiDelTag', ko.toJSON(req), false, (json) => {
            getTagList();
        });
    };
}

function clearInput(id) {
    document.getElementById(id).value = ""; 
}

self.createItem = function(name, showDetails, showEdit) {
    var item = {
        id : -1,
        name: name,
        notes: "",
        done : "",
        priority: "",
        url: "",
        showDetails: ko.observable(showDetails),
        showEdit: ko.observable(showEdit),
        showFullNotes : ko.observable(false),
        notesDisplay: ko.observable(""),
        tags: []
    }

    return item;
}

function addTodoItem() {
    var item = createItem(viewModelTodoList.addTodoItemName, true, true);
    viewModelTodoList.items().push(new ko.observable(item));
    
    viewModelTodoList.searchTerm("");
}

function addTodoItemold() {
    var req = {
        name : viewModelTodoList.addTodoItemName
    };

    xhrPost('/todo/apiUpd', ko.toJSON(req), false, (json) => {
        viewModelTodoList.addTodoItemName("");
        //clearInput('inpAddTodoItem');
        getTodoItemList();
    });
}
/*
function addTagItem() {
    var req = {
        name : viewModelTodoList.addTagItemName
    };

    xhrPost('/todo/apiAddTag', ko.toJSON(req), false, (json) => {
        
        viewModelTodoList.addTagItemName("");
        getTagList();
    });
}
*/


function addTagRelation(data) {
    var req = {
        tagId: 0,
        tagName : viewModelTodoList.addTagItemName,
        todoItemId : data.id
    };

    xhrPost('/todo/apiAddTagRelation', ko.toJSON(req), false, (json) => {
        viewModelTodoList.toggleEdit(data);
        viewModelTodoList.addTagItemName("");
        init();
        //getTagList();
    });
}

function init(userId, selectedGroupId) {
    viewModelCoinInvestmentList = new createViewModelCoinInvestmentList(selectedGroupId);
    viewModelCoinInvestmentList.userId(userId);
    ko.applyBindings(viewModelCoinInvestmentList, document.getElementById("tdCoinListId"));

    getCoinInvestmentGroupList(userId, (groups) => {
        viewModelCoinInvestmentList.investmentGroups(groups);
        var selectedGroup = getById(viewModelCoinInvestmentList.investmentGroups(), selectedGroupId);
        viewModelCoinInvestmentList.investmentGroupSelected( selectedGroup);
        refresh(viewModelCoinInvestmentList.investmentGroupSelected().id);
    });
}

function refresh(investmentGroupId, selected) {
    var userId = viewModelCoinInvestmentList.userId();
    //var groupId = investmentGroupId;

    getCoinInvestments(userId, investmentGroupId, selected, (ci) => {
        viewModelCoinInvestmentList.investmentItems(ci);
        //viewModelCoinInvestmentList.coinInvestmentList = ci;
        var i = 0;
    });
}

/*
function getTagIndexById(id) {
    var ret = null;

    for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
        var element = viewModelTodoList.tagItems()[i];
        if (element.id == id) {
            ret = i;
             
            break;
        }
    }

    return ret;
}
function getTagById(id) {
    var ret = null;

    for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
        var element = viewModelTodoList.tagItems()[i];
        if (element.id == id) {
            ret = element;
             
            break;
        }
    }

    return ret;
}
*/
function getTagNames() {
    var ret = [];

    for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
        var element = viewModelTodoList.tagItems()[i];
        ret.push(element.name);
    }

    return ret;
}

function getTagIds(tagItems) {
    var ret = [];

    for (var i = 0; i < tagItems.length; i++) {
        var element = tagItems()[i];
        ret.push(element.id);
    }

    return ret;
}

function getTagByName(name) {
    var ret = null;

    for (var i = 0; i < viewModelTodoList.tagItems.length; i++) {
        var element = viewModelTodoList.tagItems()[i];
        if (element.name == name) {
            ret = element;
             
            break;
        }
    }

    return ret;
}

function getTagsForTodoItemId(id) {
    var ret = [];

    if (id != -1) {
        viewModelTodoList.tag2TodoItems().forEach(element => {
            if (element.idtodo == id) {
                var tag = getById(viewModelTodoList.tagItems(), element.idtag);
                ret.push(tag);
            }
        });
    }
    
    return ret;
}



function getTodoItemList() {
    var apiMethod = '/todo/apiGetTodoItemList';
    
    var req = {
        tagBase:  "",
        tagSearch:  "",
        tagIds : null,
        showDone: viewModelTodoList.searchShowDone,
        showDel: viewModelTodoList.searchShowDel
    };

    if (viewModelTodoList.searchTagItems.length > 0) {
        req.tagIds = getTagIds(viewModelTodoList.searchTagItems);
        apiMethod = '/todo/apiGetTodoItemListByTags';
    }


    xhrPost(apiMethod, ko.toJSON(req), true, (json) => {
        for (var i =0 ; i < json.length; i++) {
            json[i].showDetails = ko.observable(false);
            json[i].showEdit = ko.observable(false);
            json[i].showFullNotes = ko.observable(false);
            json[i].notesDisplay = ko.observable(getTodoNotesLine(json[i].notes, false));
            json[i].tags = getTagsForTodoItemId(json[i].id);
            json[i] = ko.observable(json[i]);
            
        }
        /*json.forEach(element => {
            element.showDetails = false;

        });*/

        viewModelTodoList.items(json);
    });
}

function addInvestmentGroup() {
    var name = prompt("Name of group?");
    var req = {
        userId : viewModelCoinInvestmentList.userId(),
        name: name

    };

    xhrPost('/todo/apiCoinAddInvestmentGroup', ko.toJSON(req), true, (json) => {
        if (cb) {
            var coinInvestmentGroupList = json;
            cb(coinInvestmentGroupList);
        }
    });
}

function getCoinInvestmentGroupList(userId, cb) {
    var req = {
        userId : userId
    };

    xhrPost('/todo/apiGetCoinInvestmentGroupList', ko.toJSON(req), true, (json) => {
        if (cb) {
            var coinInvestmentGroupList = json;
            cb(coinInvestmentGroupList);
        }
    });
}


function getCoinInvestments(userId, groupId, selected, cb) {
    var req = {
        userId : userId,
        groupId : groupId,
        groupSelected : selected
    };

    xhrPost('/todo/apiGetCoinInvestmentList', ko.toJSON(req), true, (json) => {
        if (cb) {
            var coinInvestmentList = json;
            cb(coinInvestmentList);
        }
    });
}

function getTag2TodoList(cb) {
    viewModelTagList.todoItemId = -1;

    xhrPost('/todo/apiGetTag2TodoList', ko.toJSON(viewModelTagList), true, (json) => {
        viewModelTodoList.tag2TodoItems(json);
        if (cb) {
            cb(viewModelTodoList.tag2TodoItems);
        }
    });
}

var TODOSTATUS = {
	ACTIVE : 0,
	INACTIVE : 1,
	ARCHIVED : 2
};

function getTodoStatusText(status) {
    var ret = "";
    
    if (status == TODOSTATUS.INACTIVE) {
        ret = "inactive";
    } else if (status == TODOSTATUS.ARCHIVED) {
        ret = "archived";
    }

    return ret;
}

function getTodoStatusColor(status, done) {
    var ret = "colorItemActive";

    if (done) {
        ret = "colorItemDone";
    } else {
        if (status == TODOSTATUS.INACTIVE) {
            ret = "colorItemInActive";
        } else if (status == TODOSTATUS.ARCHIVED) {
            ret = "colorItemArchived";
        }
    }

    return ret;
}

function getTodoPriorityText(prio) {
    var ret = prio;
    
    if (prio<= 0) {
        ret = "";
    } else if (prio  == "null") {
        ret = "";
    }

    return ret;
}

function getTodoNotesLine(notes, showAll) {
    var maxLen = 50;
    var ret = "";

    if (showAll) {
        return getTodoNotes(notes);
    } else {
        if (notes) {
            var sp = notes.split('\n');
            if (sp.length > 0) {
                ret = sp[0]; 
                if (ret.length > maxLen) {
                    ret = ret.substr(0, maxLen) + "...";
                }
            }
        }
    }
    
    return ret;
}

function getTodoNotes(notes) {
    //replace \n with <br>
    return notes;
}

function getTodoEditUrl(id) {
    return '/todo/edit/' + id;
}


/*
function getTodoSearchUrl(tag) {
    return '/todo?searchTag=' + tag;
}*/

function getTodoSearchUrl(tag) {
    //var tag = item.name;
    return '/todo/' + tag;
}

function getCurrencyStrForValue(rate, decimals) {
    var ret = Number((rate).toFixed(decimals));
    ret = ret.toString();
    var head = "";
    var tail = "";
    var index = ret.indexOf('.');
    if (index != -1) {
        head = ret.substr(0, index);
        tail = ret.substr(index +1);

        if (tail.length < decimals) {
            for (var i = 0; i < (decimals-tail.length); i++) {
                tail += '0';
            }

            
        }
    } else {
        head = ret;
        tail = "";//0000";
        for (var i = 0; i < (decimals); i++) {
            tail += '0';
        }
    }


    
    if (head.length > 3) {
        var first = head.substr(0, head.length - 3);
        var second = head.substr(head.length - 3);
        head = first + "." + second;
    }

    ret = head + ',' + tail;

    return ret;
}

function addCurrencySymbol(value, symbol) {
    var ret = value+symbol;
    return ret;
}

function getCoinValueStr(rate, symbol) {
    var ret = "";

    if (rate != 0) {
        var val = getCurrencyStrForValue(rate, 2);
        ret = addCurrencySymbol(val, symbol);
    }

    return ret;
}

function getCoinAmountStr(rate) {
    var ret ="";

    if (rate != 0) {
        ret = getCurrencyStrForValue(rate, 4);
    }

    return ret;
}

function convertToEur(valueUSD) {
    var usdRate = 1.23;
    var eurRate = 1 / usdRate;

    var ret = valueUSD * eurRate;
    return ret;
}
