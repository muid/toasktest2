var viewModelTodoItem = {};
var viewModelNotes = {};
var viewModelTag = {};

var createViewModelTodoItem = function (id, idint, userid, username, name, url, priority, done, notes) {
    var self = this;
  
    self.id = ko.observable(id);
    self.idint = ko.observable(idint);
    self.userid = ko.observable(userid); 
    self.username = ko.observable(username); 
    self.type = ko.observable(4);
    self.status = ko.observable(0);
    self.name = ko.observable(name);
    self.url = ko.observable(url);
    self.priority = ko.observable(priority);
    self.done = ko.observable(done);
    self.notes = ko.observable(notes);
    self.taskGroupItems = ko.observable(undefined);
    self.inpCapture = ko.observable(null);
    
    self.addUpdateTaskNote = function(data, event) {
        addUpdateTaskNote(data);
    };
    
    

    
};

var createViewModelTagRelation = function (tagId, idTodoItem) {
    var self = this;
  
    self.tagId = ko.observable(tagId);
    self.todoItemId = ko.observable(idTodoItem);
    self.tagName = ko.observable("");
}

var createViewModelNotes = function (idTodoItem) {
    var self = this;
  
    self.todoItemId = ko.observable(idTodoItem);
    self.noteItems = ko.observableArray([]);

    self.addUpdateTaskNote = function(data, event) {
        addUpdateTaskNote(data);
    };

    
    self.delNote = function(data, event) {
        data.status = 2;
        addUpdateTaskNote(data, event);
    }
}


function init(userId, userName, taskId) {
    var self = this;
    

    getTaskItem(taskId, (taskItem) => {
        /*
        kbItem['showDetails'] =ko.observable(false);
        kbItem['showEdit'] = ko.observable(false),
        //kbItem['showDetails'] =false;
        //kbItem['notesDisplay'] = ko.observable(""),
        kbItem['notesDisplay'] = ko.observable(getTodoNotes(kbItem, false));

        kbItem['tags'] = [],
        //viewModelTodoItem.items.push(kbItem);
*/
        viewModelTodoItem = new createViewModelTodoItem(taskItem.id, taskItem.idint, userId, userName, taskItem.name, taskItem.url, taskItem.priority, taskItem.done, taskItem.notes);
        //viewModelTag = new createViewModelTagRelation(0, taskItem.id);
        
        getTaskGroupItems({},(items) => {
            viewModelTodoItem.taskGroupItems = ko.observable(items);
        });
    

        ko.applyBindings(viewModelTodoItem, document.getElementById("myBody"));
        //ko.applyBindings(viewModelTag, document.getElementById("tagBody"));
        

        
        Quill.prototype.getHtml = function() {
            return this.container.querySelector('.ql-editor').innerHTML;
        };

        viewModelTodoItem.inpCapture(new Quill('#bubble-container', {
            placeholder: 'Enter a note for the new todo item...',
            theme: 'snow' 
      }));

      
    var ac = autoComplete({
        selector: 'input[name="q"]',
        minChars: 0,
        onSelect: function(e, term, item){
          var k = 2;
          var tag = getTagByName(term);
          viewModelTodoList.searchTagItems.push(tag);
          document.getElementById("idTaskGroup").value = ""; 
          getTodoItemList();
          
        }, 
        onSuggestNone:  function(term) {
          //viewModelTodoList.searchTerm(term);
          var t = 0;
        },
        source: function(term, suggest){
            term = term.toLowerCase();
            var choices = viewModelTodoItem.taskGroupItems(); // ['ActionScript', 'AppleScript', 'Asp'];
            var matches = [];
            for (i=0; i<choices.length; i++) {
              var item = choices[i].name.toLowerCase();
  
              //var index  = ~choices[i].toLowerCase().indexOf(term);
              var index = item.indexOf(term);
              //if (~choices[i].toLowerCase().indexOf(term)) {
              if (index != -1) {
                  matches.push(choices[i]);
                } else {
                  if (term ==' ') {
                    matches.push(choices[i]);
                  }
                }
            }
            suggest(matches, term);
        }
      });



    });

    

    getTaskNotes(taskId, (notes) => {
        viewModelNotes = new createViewModelNotes(taskId);
        

        viewModelNotes.noteItems ( notes);
        ko.applyBindings(viewModelNotes, document.getElementById("notesBody"));

      });

}


function getTaskItem(taskId, cb) {
    var req = {
        itemId: taskId
    };

    xhrPost('/todo/apiGetTodoItemById', ko.toJSON(req), true, (json) => {
        if (cb) {
            cb(json);
        }
    });    
}

function getTaskNotes(taskId, cb) {
    var req = {
        itemId: taskId
    };

    xhrPost('/todo/apiGetNotesByTodoId', ko.toJSON(req), true, (json) => {
        if (cb) {
            cb(json);
        }
    });    
}

function addUpdateTaskNote(data) {
    var req = {
        idnote: null,
        idfk: typeof data.id == 'function' ? data.id() : data.id,
        iduser: typeof data.userid == 'function' ? data.userid() : data.userid //,
        //notes: data.inpCapture().getText(),
        //notehtml: data.inpCapture().getHtml()
    }

    if (data.inpCapture && data.inpCapture()) {
        req.notes = data.inpCapture().getText(),
        req.notehtml = data.inpCapture().getHtml()
    }
    
    addUpdateNote(req, (resp) => {
        var delta = data.inpCapture().theme.quill.clipboard.convert("");
        data.inpCapture().setContents(delta);

        getTaskNotes(req.idfk, (notes) => {
            viewModelNotes.noteItems(notes);
        });
    });
/*
    xhrPost('/todo/apiNoteUpd', ko.toJSON(req), false, (resp) => {
        getTaskNotes(req.idfk, (notes) => {
            viewModelNotes.noteItems(notes);
        });
    });
    */
}



function saveTodoItem() {
    xhrPost('/todo/apiUpd', ko.toJSON(viewModelTodoItem), false, (resp) => {
        location.href = `/${viewModelTodoItem.username()}/taskedit/${resp}/`;

    });
}



function addTagRelation() {
    xhrPost('/todo/apiAddTagRelation', ko.toJSON(viewModelTag));
}

function delTagRelation(tagId) {
    viewModelTag.tagId = tagId;

    xhrPost('/todo/apiDelTagRelation', ko.toJSON(viewModelTag));
}

