function xhrPost(url, json, parseJson, cb) {
    fetch(url, {
        method: 'post',
        headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        },
        body: json
    }).then( (r) => {
        if (parseJson) {
            r.json().then( (resp) => {
                var i = resp;
                if (cb) {
                    cb(resp);
                }
            })
        } else {
            r.text().then( (resp) => {
                console.log(resp);
                if (cb) {
                    cb(resp);
                }
            })
        }
    })
        
        
/*.then(obj => console.log(obj));
    .then(function (response) {
        if (cb) {
            cb();
        }
        console.log(response);
    })*/
    .catch(function (error) {
        console.error(error);
    });
}

function checkAndFixUrl(url) {
    var ret = url;
    if (typeof(url) =='function') {
        ret = ret();
    }

    if (ret) {
        if (ret.indexOf("http") == -1) {

            ret = "http://" + url;
            if (typeof(url) == 'function') {
                ret = url(ret);
            }
        }
    }

    return ret;
}

function shortenString(str, maxLen) {
    var ret = str;

    if (typeof(str) =='function') {
        ret = ret();
    }

    if (ret) {
        if (ret.indexOf('https:')) {
            ret = ret.substr("https://".length+1);
        } else if (ret.indexOf('http:')) {
            ret = ret.substr("http://".length+1);
        }

        if (str.length > maxLen) {
            var half = maxLen /2;
            var part1 = ret.substr(0, half);
            var part2 = ret.substr(ret.length+3-half, half-3);
            ret = `${part1}...${part2}`;
        }
    }

    return ret;
}

function getIndexById(arr, id) {
    var ret = -1;

    var myArr = arr;
    if (typeof arr == "function") {
        myArr = arr(); //is this a knockout observable?
    }

    for (var i = 0; i < myArr.length; i++) {
        var element = myArr[i];
        if (element.id == id) {
            ret = i;
             
            break;
        }
    }

    return ret;
}

function getById(arr, id) {
    var ret = null;

    var index = getIndexById(arr, id);
    if (index != -1) {
        ret = arr[index];
    }

    return ret;
}

function createItemBase(data, type, event) {
    var req = {
        idnote: data.idnote,
        iduser: data.iduser,
        type: type
    };

    xhrPost('/todo/apiJournalCreateItem', ko.toJSON(req), true, (json) => {
        if (cb) {
            cb(json);
        }
    });    

    return null;
}

function createKnowledgebaseItem(data, event) {
    return createItemBase(data, 2, event);
}

function createTaskItem(data, event) {
    return createItemBase(data, 4, event);
}


function createTodoItem(data, event) {
    return createItemBase(data, 0, event);
}

function addUpdateNote(data, cb) {
    xhrPost('/todo/apiNoteUpd', ko.toJSON(data), false, (resp) => {
        if (cb) {
            cb();
        } 
    });
}

function getTaskGroupItems(data, cb) {
    xhrPost('/todo/apiGetTaskGroupList', ko.toJSON(data), true, (resp) => {
        if (cb) {
            cb(resp);
        } 
    });
}

/*
function createTodoItem(data, event) {
    var req = {
        idnote: data.idnote,
        iduser: data.iduser
    };

    xhrPost('/todo/apiJournalCreateTodoItem', ko.toJSON(req), true, (json) => {
        if (cb) {
            cb(json);
        }
    });    

    return null;
}
*/


function getTodoNotes(data) {
    //replace \n with <br>
    var ret = data;
    if (data && data.notehtml) {
        ret = data.notehtml;
    } else if (data && data.notes) {
        ret = data.notes;
    }

    return ret;
}

function getUserName(userId, userList) {
    var ret = "";

    if (userList) {
        var user = getById(userList, userId);
        if (user && user.id) {
            ret = user.name;
        }
        }

    return ret;
}

function getCreatedModifiedStr(userIdCreated, dateCreated, userIdModified, dateModified) {
    var ret = ""; 
    //var u = getById(viewModelTodoList.userList(), userIdCreated);
    if (userIdCreated) {
        ret = `Created by ${getUserName(userIdCreated)} at ${utils.getDateTimeStrFromMS(dateCreated)}`;
        
        if (!(userIdModified == userIdModified && dateCreated == dateModified)) {
            ret += `, modified by ${getUserName(userIdModified)} at ${utils.getDateTimeStrFromMS(dateModified)}`;
        }
    }

    return ret;
}

function showElement(elem, show) {
    
  //  var paragraph = elem.querySelector(".hidden");
    if (elem.style.display == "none") {
        elem.style.display = "block";
    } else {
        elem.style.display = "none";
    }
}
