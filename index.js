const express = require('express');
//var session = require('express-session');
//const sqliteStoreFactory = require('express-session-sqlite');
//const SqliteStore = sqliteStoreFactory.default(session);
const sqlite = require('sqlite3');
const path = require('path');
var https = require('https');
var http = require('http');
var uuid = require('uuid');
const config = require('./config'); 
const utils = require('./public/shared/utils'); 

//const { getMaxListeners } = require('process');
//const request = require('request');
const fileUpload = require('express-fileupload');
const cors = require('cors');
const bodyParser = require('body-parser');

const fs = require('fs');
const { O_DIRECTORY } = require('constants');

var mammoth = require("mammoth");
var striptags = require('striptags');
var md5  = require("md5");

const app = express();

var port = 8000;
if (__dirname.indexOf("beta") != -1) {
	port = 8080;
}

var db = null 
var allTags = [];
var allUsers = [];

app.set('views', './views');
app.set('view engine', 'pug');
//app.use(express.json());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.use(fileUpload({
    createParentPath: true
}));

function convertDocxToHtml(inputDocFilePathWithFileName, cb) {
	mammoth.convertToHtml({
		path: inputDocFilePathWithFileName
	  }, null)
	.then(function (result) {
		var html = result.value; // The generated HTML 
		if (cb) {
			cb(result);
		}
		
		var messages = result.messages; // Any messages, such as warnings during conversion 
		console.log(messages);
	})
	.done();
}
  
if (fs.stat(config.dbPath, (error, stats) => {
	if (error) {
		fs.copyFileSync('./db/todo.db', config.dbPath);
		//console.log(error);
	}
}));

/*  
app.use(session({
    store: new SqliteStore({
      // Database library to use. Any library is fine as long as the API is compatible
      // with sqlite3, such as sqlite3-offline
      driver: sqlite.Database,
      // for in-memory database
      // path: ':memory:'
      path: '/session.db',
      // Session TTL in milliseconds
      ttl: 1234,
      // (optional) Session id prefix. Default is no prefix.
      prefix: 'sess:',
      // (optional) Adjusts the cleanup timer in milliseconds for deleting expired session rows.
      // Default is 5 minutes.
      cleanupInterval: 300000
	}),
	secret: 'yyy',
	key: 'test',
	proxy: 'false',
	saveUninitialized : false,
	resave : false
    //... don't forget other expres-session options you might need
}));
*/
/*
app.use(session({
	secret: 'yyy',
	key: 'test',
	proxy: 'false',
	saveUninitialized : false,
	resave : false,
	store: new MemcachedStore({
		hosts: [process.env.MEMCACHE_URL || '127.0.0.1:11212']
	})
}));
*/
//app.use(express.static(__dirname + '/public' ));
app.use('/public', express.static(path.join(__dirname, 'public')))
 
/*
app.get('/member/:name/planet/:home', (req, res) => {
	const memberDetails = {
		member: req.params.name,
		planet: req.params.home
	}
	res.render('guardian', memberDetails);
});

app.get('*', (req, res, next) => {
	res.status(200).send('Sorry, page not found');
	next();
});


app.get('/todo.css', (req, res) => {
	res.status(200).header(m).sendFile(__dirname +  '/todo.css');
});
*/

function createKBFromDocx(filePath) {
	var fileName = path.basename(filePath);
	
 
	convertDocxToHtml(filePath, (res) => {
		var notesHtml = escapeText(res.value);
		var notesText = striptags(notesHtml);
		
		var item = {
			id : uuidCreate(),
			userid: uuidCreateEmpty(),
			idnote: uuidCreateEmpty(),
			type: TODOTYPE.KB,
			name: fileName,
			notes: notesText,
			notehtml: notesHtml,
			done : 0,
			priority: 0,
			status: TODOSTATUS.ACTIVE,
			url: "",
			duedate: 0
		};

		todoAddOrUpdate(item.id, item.userid, null, item.type, item.name, item.notes, item.url, item.done, item.priority, item.status, item.duedate, (lastId) => {
			if (lastId) {
				noteAddOrUpdate(item.idnote, lastId, item.userid, 1, 0, item.notes, item.notehtml, (lastIdNote) => {
					todoUpdateNoteId(lastId, lastIdNote, () => {
						if (cb) {
							cb();
						}
					});
				});
			}
		});
	});
}

function userGetByName(name, cb) {
	var sql = `SELECT ${getSQLValue("id", SQLTYPE.UUID, "u") } as id, u.name as name FROM user u`;
	//var sql = `SELECT ${getSQLValue("id", SQLTYPE.NUMBER, "u") } as id, u.name as name FROM user u`;
	sql += ` WHERE u.name = '${name}'`;

	//console.error("userGetByName");

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				if (rows.length > 0) {

					//updateId("user", rows[0].id, "uid");
					cb(rows[0]);
				} else {
					
					cb(null);
				}
			}
		}
	});
}

function userGetById(id, cb) {
	//var sql = `SELECT ${getSQLValue("id", SQLTYPE.UUID, "u") } as id, u.name as name FROM user u`;
	var sql = `SELECT id, ${getSQLValue("uid", SQLTYPE.UUID, "u") } as uid, u.name as name FROM user u`;
	sql += ` WHERE u.id = '${id}'`;

	//console.error("userGetByName");

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				if (rows.length > 0) {

					//updateId("user", rows[0].id, "uid");
					cb(rows[0]);
				} else {
					
					cb(null);
				}
			}
		}
	});
}

function userGetAll(cb) {
	//var sql = `SELECT  ${getSQLValue("uid", SQLTYPE.UUID)}, name, id FROM user u`;
	var sql = `SELECT  ${getSQLValue("id", SQLTYPE.UUID)}, name FROM user u`;

	//sql += ` WHERE u.name = '${name}'`;

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			allUsers = rows;
			if (cb && rows.length > 0) {
				
				cb(rows);
			}
		}
	});
}

function userGetById(userId) {
    var ret = null;

    var user = utils.getById(allUsers, userId);
    if (user && user.id) {
        ret = user;
    }

    return ret;
}

function userGetNames(cb) {
	var sql = `SELECT  ${getSQLValue("id", SQLTYPE.UUID, "u") } as id, u.name as name FROM user u`;

	//sql += ` WHERE u.name = '${name}'`;

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb && rows.length > 0) {
				cb(rows);
			}
		}
	});
}

app.get('/', (req, res, next) => {
	/*if(req.session.views) {
        ++req.session.views;
    } else {
        req.session.views = 1;
	}*/

	userGetNames((userNames) => {
		res.render('index', { userNames: userNames });
	});
	//res.render('index');
});

app.get('/:user', (req, res) => {
	
	var userName = req.params.user;
	//req.session.userName = userName;
	//req.session.loggedIn = true;

	//return res.redirect(`/${userName}/todo`);
	var availableApps = [];
	availableApps.push("todo");
	availableApps.push("tasks");
	//availableApps.push("coins");
	availableApps.push("journal");
	availableApps.push("kb");
	availableApps.push("unsorted");
	res.render('apps', { username: userName, apps: availableApps });

	//return res.redirect(`/${userName}/coins`);
});
/*
app.get('/todo', (req, res) => {
	
var i = 0;
	//getList(0, 0, res);
});
*/

app.post('/:user/upload-file/:id', async (req, res) => {
    try {
		var param = req.params;

        if(!req.files) {
            res.send({
                status: false,
                message: 'No file uploaded'
            });
        } else {
            //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
            let fileObj  = req.files.newFile;
			var dir = "./uploads/"+param.id;
			if (!fs.existsSync(dir)) {
				fs.mkdirSync(dir);
			}
            //Use the mv() method to place the file in upload directory (i.e. "uploads")
            fileObj.mv(dir + '/' + fileObj.name);

			//send response
			res.status(200).send('');
            /*res.send({
                status: true,
                message: 'File is uploaded',
                data: {
                    name: fileObj.name,
                    mimetype: fileObj.mimetype,
                    size: fileObj.size
                }
            });*/
        }
    } catch (err) {
        res.status(500).send(err);
    }
});


app.get('/:user/todo', (req, res) => {
	/*if (!req.session.loggedIn) {
		return res.redirect(`./`);
	}*/
	
	//updateIdIntForTable("todo",TODOTYPE.TODO);

	//updateIdForTable("todo", "uid");
	//updateUserIdForTable("CoinInvestmentGroup", "uiduser");
	//updateUserIdForTable("CoinInvestment", "uiduser");
	//updateUserIdForTable("Todo", "idusercreated", "uidusercreated");
//return ;

	kbSyncItems();

	var userName = req.params.user;

	if (userName) {
		userGetByName(userName, (user) => {
			if (user) {
				//updateIdIntForTable("todo", 0);
				res.render('todolist', { userid: user.id, username: user.name  });
			}
		});

	}
});

app.get('/:user/tasks', (req, res) => {
	/*if (!req.session.loggedIn) {
		return res.redirect(`./`);
	}*/
	
	var userName = req.params.user;

	if (userName) {
		userGetByName(userName, (user) => {
			if (user && user.id != -1) {
				res.render('tasklist', { userid: user.id, username: user.name  });
			}
		});
	}
});

app.get('/:user/journal', (req, res) => {
	/*if (!req.session.loggedIn) {
		return res.redirect(`./`);
	}*/
	
	var userName = req.params.user;

	if (userName) {
		userGetByName(userName, (user) => {
			if (user && user.id != -1) {
				//updateIdIntForTable("todo", 1);
				res.render('journallist', { userid: user.id, username: user.name  });
			}
		});
	}
});

app.get('/:user/unsorted', (req, res) => {
	/*if (!req.session.loggedIn) {
		return res.redirect(`./`);
	}*/
	
	var userName = req.params.user;

	if (userName) {
		userGetByName(userName, (user) => {
			if (user && user.id != -1) {
				res.render('unsortedlist', { userid: user.id, username: user.name  });
			}
		});
	}
});

app.get('/:user/kb', (req, res) => {
	/*if (!req.session.loggedIn) {
		return res.redirect(`./`);
	}*/
	var f= "d:/dev/nodejs/test1/input.docx";

	
	//createKBFromDocx(f);

	var userName = req.params.user;

	if (userName) {
		userGetByName(userName, (user) => {
			if (user && user.id != -1) {
				//updateIdIntForTable("todo", 2);
				res.render('kblist', { userid: user.id, username: user.name  });
			}
		});
	}
});

app.get('/:user/coins', (req, res) => {
	/*if (!req.session.loggedIn) {
		//return res.redirect(`./`);
	}*/
	var selectedGroup = 0;
	var userName = req.params.user;
	if (userName) {
		userGetByName(userName, (user) => {
			if (user && user.id) {
				coinGetGroups(user.id, (groups) => {
					if (groups.length > 0) {
						selectedGroup = groups[0].id;
					}

					httpGetCoinRate( (rates) => {
						if (rates) {
							var btc = rates.data.BTC;
							var btcPrice = btc.quote.USD.price;
	
							var eth = rates.data.ETH;
							var ethPrice = eth.quote.USD.price;
	
							var doge = rates.data.DOGE;
							var dogePrice = doge.quote.USD.price;
					
							var d = new Date().getTime();
	
							coinInsertRate(1, btcPrice, d, () => {
								coinInsertRate(2, ethPrice, d, () => {
									coinInsertRate(4, dogePrice, d, () => {
										res.render('coinlist', { userid: user.id, username: user.name, selectedGroup: selectedGroup  });
									});	
								});	
							});
						} else {
							res.render('coinlist', { userid: user.id, username: user.name, selectedGroup: selectedGroup  });
						}
					});
				});
				

				
			}
		});

	}
	


	
});
/*
app.get('/todo/:tagBase', (req, res) => {
	var tb = req.params.tagBase;

	var searchTags = [];
	var split = tb.split("+");
	if (split.length > 1) {
		for (var i = 0; i < split.length; i++) {
			searchTags.push(split[i]);
		}
		var ik=0;
	} else {
		searchTags.push(tb);
	}

	res.render('todolist', { todoitems: null, tags: searchTags  });
	// tagGetItems((tags) => {
	// 	var tagId = tagExists(tb);
	// 	if (tagId != -1) {
	// 		getList(tagId, 0, res);
	// 	} else {
	// 		res.status(200).send('Sorry, page not found');
	// 	}
	// });
});
*/

app.get('/todo/:tagBase/:tagSearch', (req, res) => {
	var tb = req.params.tagBase;
	var ts = req.params.tagSearch;
	var i = 0;
	getList(tb, ts, res);
});

app.get('/:user/todoedit/:todoId', (req, res) => {
	var todoId = req.params.todoId;
	todoId = parseInt(todoId);

	if (todoId) {
		tagGetItems((tags) => {
			tagGetItemsForTodoItem(todoId, (itemTags) => {
				todoGetItem(todoId, (todoItem) => {
					res.render('todoedit', { todoitem: todoItem, tags: tags, itemTags: itemTags, user: user.n  })
				});
			});
		});
	} else {

	}
	//getList(0, 0, res);
});

app.get('/:user/taskedit/:taskId', (req, res) => {
	var taskId = req.params.taskId;
	//taskId = parseInt(taskId);
	if (!taskId ||taskId =='0') {
		taskId = uuidCreateEmpty();
		//taskId = uuidCreate();
		//taskId = uuidToShortUuid(taskId);
	}

	if (taskId) {
		var userName = req.params.user;

		if (userName) {
			userGetByName(userName, (user) => {
				if (user && user.id != -1) {
					tagGetItems((tags) => {
						tagGetItemsForTodoItem(taskId, (itemTags) => {
							todoGetItem(taskId, (todoItem) => {
								if (todoItem) {
									res.render('taskedit', { todoitem: todoItem, tags: tags, itemTags: itemTags, user: user  })
								} else 
								{
									res.status(200).send('');
								}
								
							});
						});
					});
				} else {
			
				}
				
			});
		}
	}
	//getList(0, 0, res);
});

app.get('/:user/kbedit/:kbId', (req, res) => {
	var kbId = req.params.kbId;
	//taskId = parseInt(taskId);

	if (kbId) {
		var userName = req.params.user;

		if (userName) {
			userGetByName(userName, (user) => {
				if (user && user.id != -1) {
					tagGetItems((tags) => {
						tagGetItemsForTodoItem(kbId, (itemTags) => {
							todoGetItem(kbId, (todoItem) => {
								todoItem.kbId =  utils.createIdStrForIdInt(todoItem.idint, 2);
								res.render('kbedit', { todoitem: todoItem, tags: tags, itemTags: itemTags, user: user  })
							});
						});
					});
				} else {
			
				}
				
			});
		}
	}
	//getList(0, 0, res);
});


app.get('/:user/noteview/:noteId', (req, res) => {
	var noteId = req.params.noteId;

	if (noteId) {
		noteGetItem(noteId, (noteItem) => {
			res.render('noteview',  noteItem  )
		});
	} else {

	}
	//getList(0, 0, res);
});



app.post("/todo/apiUpd", function(req, res)  { 
	var item = req.body;

	todoAddOrUpdate(item.id, item.userid, null, item.type, item.name, item.notes, item.url, item.done, item.priority, item.status, item.duedate, (lastId) => {
		if (lastId) {
			if (item.notes && item.notes.length > 0) {
				noteAddOrUpdate(item.idnote, lastId, item.userid, 1, 0, item.notes, item.notehtml, (lastIdNote) => {
					todoUpdateNoteId(lastId, lastIdNote, () => {
						if (cb) {
							cb(lastId);
						}
					});
					
					res.status(200).send(uuidToShortUuid(lastId));
				});
			} else {
				res.status(200).send(uuidToShortUuid(lastId));
			}

		
		
/*
			noteAddOrUpdate(-1, lastId, item.userid, 1, 0, item.notes, item.notesHTML, (lastIdNote) => {
				todoUpdateNoteStatus(lastIdNote, TODONOTESTATUS.OBSOLETE, () => {
					todoUpdateNoteId(lastId, lastIdNote, () => {
						res.status(200).send('');	
					});
				});
				
			});*/
		} else {
			res.status(200).send('');
		}
	});
});

app.post("/todo/apiJournalUpd", function(req, res)  { 
	var item = req.body;
	
	journalGetItem(item.id, item.duedate, (itemFound) => {
		if (itemFound) {
			
			if (item.onlyUpdateNote) {
				 
				if (item.type == TODOTYPE.UNSORTED) {
					noteUpdateTodoId(item.idnote, item.idjournal, TODOTYPE.JOURNAL, () => {
						res.status(200).send('');
					}); 
				} else
				if (item.status == TODONOTESTATUS.DELETED) {
					todoUpdateNoteStatus(item.idnote, item.status, () => {
						res.status(200).send('');
					}); 
				} else {
					noteUpdateText(item.idnote, item.notes, item.notehtml, item.userid, () => {
						res.status(200).send('');
					}); 
				}
				
			} else {
				noteAddOrUpdate(item.idnote, itemFound.id, item.userid, 1, 0, item.notes, item.notehtml, () => {
					res.status(200).send('');
				});
			}
		} else {
			todoAddOrUpdate(item.id, item.userid, null, item.type, item.name, item.notes, item.url, item.done, item.priority, item.status, item.duedate, (lastId) => {
				if (lastId && item.notes && item.notes.length > 0) {
					noteAddOrUpdate(item.idnote, lastId, item.userid, 1, 0, item.notes, item.notehtml, () => {
						res.status(200).send('');
					});
				} else {
					res.status(200).send('');
				}
			});
		}
	});
});

app.post("/todo/apiNoteUpd", function(req, res)  { 
	var item = req.body;

	noteAddOrUpdate(item.idnote, item.idfk, item.userid, 1, 0, item.notes, item.notehtml, () => {
		res.status(200).send('');
	});

});

app.post("/todo/apiAddTagRelation", function(req, res)  { 
	var item = req.body;

	if (item.tagId == 0) {
		tag2TodoAdd(item.todoItemId, item.tagName);
	} else {
		tag2TodoAddDb(item.todoItemId, item.tagId);
	}

	res.status(200).send('');
});

app.post("/todo/apiDelTagRelation", function(req, res)  { 
	var item = req.body;

	if (item.tagId > 0) {
		tag2TodoDel(item.todoItemId, item.tagId);
	}

	res.status(200).send('');
});

app.post("/todo/apiDelTodoItem", function(req, res)  { 
	var item = req.body;

	if (item.id != -1) {
		todoDel(item.id);
	}

	res.status(200).send('');
});

app.post("/todo/apiDisableTodoItem", function(req, res)  { 
	var item = req.body;

	if (item.id != -1) {
		todoSetStatus(item.id, TODOSTATUS.INACTIVE);
	}

	res.status(200).send('');
});

app.post("/todo/apiArchiveTodoItem", function(req, res)  { 
	var item = req.body;

	if (item.id != -1) {
		todoSetStatus(item.id, TODOSTATUS.ARCHIVED);
	}

	res.status(200).send('');
});

app.post("/todo/apiAddTag", function(req, res)  { 
	var item = req.body;

	if (item.name) {
		tagAddOrUpdate(null, null, 0, "", item.name, null);
	}

	res.status(200).send('');
});

app.post("/todo/apiDelTag", function(req, res)  { 
	var item = req.body;

	if (item.tagId != -1) {
		tagDel(item.tagId);
	}

	res.status(200).send('');
});

app.post("/todo/apiGetTaskGroupList", function(req, res)  { 
	var params = req.body;
	var userId = params.userId;

	taskGroupGetItems( (items) => {
		var json = JSON.stringify(items);
		res.status(200).send(json);
	})
});




app.post("/todo/apiGetCoinRatesList", function(req, res)  { 
	var params = req.body;
	var userId = params.userId;
});

app.post("/todo/apiGetCoinInvestmentGroupList", function(req, res)  { 
	var params = req.body;
	var userId = params.userId;
	var ret = [];

	coinGetGroups(userId, (items) => {
		if (items && items.length > 0) {
			for (var i = 0; i < items.length; i++) {
				var item = items[i];
				var dateStr = "";

				if (item.datecreated && item.datecreated > 0) {
					dateStr = utils.getDateStrFromMS(item.datecreated);
					//var date = new Date(item.datecreated).tisg;
					//dateStr = date..toDateString();
				}
	
				var entry = {
					id : item.id,
					name :  `${item.name} - ${dateStr}`
				}
				
				ret.push(entry); 
			}
		} else {
			var entry = {
				id : 0,
				name :  `Current`
			}
			
			ret.push(entry); 
		}
			
		

		var json = JSON.stringify(ret);
		res.status(200).send(json);
	});
});

app.post("/todo/apiCoinAddInvestmentGroup", function(req, res)  { 
	var params = req.body;


	coinAddInvestmentGroup(params.userId, params.name);
});

app.post("/todo/apiGetCoinInvestmentList", function(req, res)  { 
	var params = req.body;
	var userId = params.userId;
	var groupId = params.groupId;
	var selected = params.groupSelected;

	if (selected) {
		coinGroupSetLastUsed(userId, groupId);
	}
	coinGetItems(userId, groupId, (items) => {
		if (items && items.length > 0) {
			var sumvaluecurrent = 0;
			var sumgainsvalue = 0;
			var sumvalueinitial = 0;
			var sumvalueinitialEUR = 0;

			for (var i = 0; i < items.length; i++) {
				items[i].valuecurrent = items[i].amount * items[i].rate;
				items[i].gainspercent = (items[i].valuecurrent / items[i].valueinitial -1)*100;
				items[i].gainsvalue = items[i].valuecurrent - items[i].valueinitial;

				sumvaluecurrent += items[i].valuecurrent;
				sumvalueinitial += items[i].valueinitial;
				sumvalueinitialEUR += items[i].valueinitialEUR;
				sumgainsvalue += items[i].gainsvalue;
			}

			var entry = cloneObject(items[0]); 
			entry.name ="SUM";
			entry.amount = 0;
			entry.rate =0;
			entry.date = 0;
			entry.valueinitial = 0;
			entry.valueinitialEUR = 0;
			entry.valuecurrent = sumvaluecurrent;
			entry.valueinitial = sumvalueinitial;
			entry.valueinitialEUR = sumvalueinitialEUR;
			entry.gainspercent = (sumvaluecurrent / sumvalueinitial -1)*100;
			entry.gainsvalue =  sumgainsvalue;

			
			items.push(entry);
		}

		var json = JSON.stringify(items);
		res.status(200).send(json);
	});
});


app.post("/todo/apiGetTodoItemList", function(req, res)  { 
	var params = req.body;
	var userId = params.userId;
	var showDone = params.showDone;
	var type = params.type;

	var statusSet = 0;
	if (params.showDel) {
		statusSet = TODOSTATUS.INACTIVE;
	}

	var tagBase = params.tagBase;
	var tagSearch = params.tagSearch;
	var tags = null;
	var searchTerms = [];
	if (params.searchTerm && params.searchTerm.length > 0) {
		searchTerms = params.searchTerm.split(' ');
	} 

	todoGetItems(userId, type, searchTerms, tags, tagBase, tagSearch, showDone, statusSet, (todoItems, tags) => {
		var json = JSON.stringify(todoItems);
		res.status(200).send(json);
	});
});

app.post("/todo/apiGetTodoItemListByTags", function(req, res)  { 
	var params = req.body;
	var userId = params.userId;
	var showDone = params.showDone;
	var type = params.type;
	
	var statusSet = 0;
	if (params.showDel) {
		statusSet = TODOSTATUS.INACTIVE;
	}

	var tagIds = params.tagIds;
	var tagIdsExclude = params.tagIdsExclude;
	var searchTerms = [];
	if (params.searchTerm && params.searchTerm.length > 0) {
		searchTerms = params.searchTerm.split(' ');
	} 

	todoGetItems2(userId, type, searchTerms, tagIds, tagIdsExclude, showDone, statusSet, (todoItems) => {
		var json = JSON.stringify(todoItems);
		res.status(200).send(json);
	});
});


app.post("/todo/apiGetTodoItemById", function(req, res)  { 
	var  params = req.body;
	var itemId = params.itemId;

	todoGetItem(itemId, (todoItem) => {
		noteGetItem(todoItem.idnote, (note) => {
			todoItem.notes = "";
			todoItem.notehtml = "";
			if (note) {
				todoItem.notes = note.note;
								
				if ((note.notehtml && note.notehtml.length == 0) || !note.notehtml) {
					todoItem.notehtml = note.note;
				} else {
					todoItem.notehtml = note.notehtml;
				}
				}
			var json = JSON.stringify(todoItem);
			res.status(200).send(json);
		});
		
	});	
});

app.post("/todo/apiGetTodoItemByIdInt", function(req, res)  { 
	var  params = req.body;
	var idInt = params.intId;
	var type = params.type;

	getIdByIdInt("todo", idInt, type, (item) => {
		var i = item;
		var json = JSON.stringify(item);
		res.status(200).send(json);
	}); 
});

app.post("/todo/apiGetNotesByTodoId", function(req, res) {
	var  params = req.body;
	var itemId = params.itemId;


	noteGetItemsFor(itemId, (noteItems) => {
		var json = JSON.stringify(noteItems);
		res.status(200).send(json);
	});
});

function getTodoNotes(data, preferText) {
    //replace \n with <br>
	var ret = "";
	
	if (data) {
		ret = data.notehtml;
		
		if (preferText) {
			ret = data.note;
		}

		if (!ret) {
			ret = data.note;

			if (preferText &&  data.notehtml) {
				ret = data.notehtml;
			}
		}
	}

	if (!ret) {
		ret = "";
	}

    return ret;
}

app.post("/todo/apiJournalCreateItem", function(req, res)  { 
	var params = req.body;
	var userId = params.iduser;
	var noteId = params.idnote;
	var itemType = params.type;
	

	noteGetItem(noteId, (item) => {
		var name = getTodoNotes(item, true);
		if (name) {
			name = name.substr(0, 50);
		}

		todoAddOrUpdate(null, userId, noteId, itemType, name, "", "", 0, 0, 0, 0, (todoId) => {
			noteUpdateTodoId(noteId, todoId, itemType); 

			res.status(200).send("");
		}); 
	});
});

app.post("/todo/apiGetJournalItemList", function(req, res)  { 
	var ret = [];

	var params = req.body;
	var userId = params.userId;
	var showDone = params.showDone;
	var type = params.type;
	var typeOrg = params.type;

	if (type == TODOTYPE.UNSORTED) {
		type = TODOTYPE.JOURNAL;
	}
	//var searchShowDel = params.showDel;

	var statusSet = 0;
	if (params.showDel) {
		statusSet = TODOSTATUS.INACTIVE;
	}

	var tagBase = params.tagBase;
	var tagSearch = params.tagSearch;
	var tags = null;

	todoGetItems(userId, type, "", tags, tagBase, tagSearch, showDone, statusSet, (journalItems, tags) => {
		async function asyncRunner () {
			try {
				//updateIdIntForTable("note", -1);
				for (var i = 0; i < journalItems.length; i++) {
					//if (!rows[i].uid)
					await noteGetItemsForP(journalItems[i].id, (notes) => {
						for (var j = 0; j < notes.length; j++) {
							
							var note = notes[j];
							if (typeOrg == TODOTYPE.UNSORTED)  {
								if (note.idtodo || note.idjournal || note.idkb || note.idtask) {
									continue;
								}
							}

							if (note.status == TODONOTESTATUS.DELETED) {
								continue;
							}

							var itemBase = cloneObject(journalItems[i]);
							itemBase['mainEntry'] = false;
							if (j==0) {
								itemBase.mainEntry = true;
							}

							itemBase.idnote = note.id;
							itemBase.idkb = note.idkb;
							itemBase.idtodo = note.idtodo;
							itemBase.idtask = note.idtask;
							itemBase.idjournal = note.idjournal;
							
							
							itemBase.notes = note.note;
							
							if ((note.notehtml && note.notehtml.length == 0) || !note.notehtml) {
								itemBase.notehtml = note.note;
							} else {
								itemBase.notehtml = note.notehtml;
							}

							itemBase.datemodified = note.datemodified;
							itemBase.idusermodified = note.idusermodified;
							itemBase.datecreated = note.datecreated;
							itemBase.idusercreated = note.idusercreated;


							ret.push(itemBase);
						}
					});

					for (var k = 0; k < ret.length; k++) {
						var item = ret[k];

						if (!item.idintkb) {
							item.idintkb = -1;

							if (item.idkb) {
								await todoGetItemP(item.idkb, (todoItem) => {
									item.idintkb = todoItem.idint;
								});
							}
						}

						if (!item.idinttodo) {
							item.idinttodo = -1;

							if (item.idtodo) {
								await todoGetItemP(item.idtodo, (todoItem) => {
									item.idinttodo = todoItem.idint;
								});
							}
						}

						if (!item.idinttask) {
							item.idinttask = -1;

							if (item.idtask) {
								await todoGetItemP(item.idtask, (todoItem) => {
									item.idinttask = todoItem.idint;
								});
							}
						}
						
					} 
					//console.log("updateIdForTable:" + i + ", " + journalItems[i].name);
				}


			} catch (error) {
				console.error(error)
			}

			var json = JSON.stringify(ret);
			res.status(200).send(json);
		};

		asyncRunner();
		/*for (var i = 0; i < journalItems.length; i++) {
			
			noteGetItemsFor(journalItems[i].idfk, (notes) => {
				for (var j = 0; j < notes.length; j++) {
					var itemBase = cloneObject(journalItems[i]);
					itemBase.notehtml = notes.notehtml;
					itemBase.notes = notes.note;
					itemBase.datemodified = notes.datemodified;
					itemBase.idusermodified = notes.idusermodified;
					itemBase.datecreated = notes.datecreated;
					itemBase.idusercreated = notes.idusercreated;
					ret.push(itemBase);
				}
				
				//journalItems[i].notes = notes;
			});
		}

		var json = JSON.stringify(journalItems);
		res.status(200).send(json);*/
		
		//res.render('todolist', { todoitems: todoItems, tags: tags  })
	});
});

app.post("/todo/apiGetUserList", (req, res) => {
	userGetNames( (users) => {
		var json = JSON.stringify(users);
		res.status(200).send(json);
	});
});

app.post("/todo/apiGetTagList", function(req, res)  { 
	var item = req.body;

	tagGetItems((tags) => {
		var json = JSON.stringify(tags);
		res.status(200).send(json);
	});
});
app.post("/todo/apiGetTag2TodoList", function(req, res)  { 
	var item = req.body;

	tag2TodoGetItems((tag2TodoList) => {

		var json = JSON.stringify(tag2TodoList);
		res.status(200).send(json);
	});
});
function GetTitleFromUrl(url, forceHttps, cb) {
	var ret = "";

	if (!utils.isUrl(url)) {
		cb("");
		return;
	}

	var buff = Buffer.alloc(0);

	var httpFunc = http.get;
	if (forceHttps || url.indexOf("https:") != -1) {
		httpFunc = https.get;
	}

	if (forceHttps) {
		if (url.indexOf("https:") == -1) {
			var index = url.indexOf("http:");
			if (index != -1) {
				url = "https:" + url.substr(5);
			} else {

			}
		}
	}
	
	httpFunc(url, function(res) {
		var pass = 1;
		var lastPass = false;
		var maxSize = 100000;
		if ((url.toLowerCase().indexOf("amazon") != -1) ||
			(url.toLowerCase().indexOf("youtube") != -1)) {
			maxSize = 3000000;
		}

		res.on('data', function(chunk) {
			buff = Buffer.concat([buff, chunk]);
			var tmpChunkStr = chunk.toString().toLowerCase();
			var tmpHtmlStr = buff.toString().toLowerCase();
			var titleFound = false;
			var htmlFound = false;
			var rHtml = /<html(\s|.)*?>/;
			var rTitle = /<title>/;
			if (rHtml.test(tmpHtmlStr) || htmlFound) {
				htmlFound = true;

				if (rTitle.test(tmpHtmlStr)) {
					if (tmpHtmlStr.length < 5000) {
						if (tmpHtmlStr.indexOf("301") == -1 && tmpHtmlStr.indexOf("moved") == -1 && tmpHtmlStr.indexOf("permanently") == -1) {
							titleFound = true;
						}
					} else {
						titleFound = true;
					}

					res.destroy();
					lastPass = true;
				}
			}
			/*
			if ((tmpHtmlStr.indexOf("<html>") != -1 || tmpHtmlStr.indexOf("<html ") != -1) && tmpHtmlStr.indexOf("<title>") != -1) {
				res.destroy();
				if (tmpHtmlStr.indexOf("301") == -1 && tmpHtmlStr.indexOf("moved") == -1 && tmpHtmlStr.indexOf("permanently") == -1) {
					found = true;
				}
			} else if (buff.length > 3000) {
				res.destroy();
			}
			*/	
				
			/*
				if (tmpHtmlStr.indexOf("<title>") != -1 || tmpHtmlStr.indexOf("<TITLE>") != -1) {
					res.destroy();
					found = true;
				}*/
				
			

			if (titleFound) {
				var tmpHtmlStr = tmpHtmlStr.replace(/[\n\r]/g,' ');
				var m = tmpHtmlStr.match(/<title>((.*))<\/title>/g);
				if (m.length > 0) {
					ret = m[0];
					ret = ret.replace("<title>","");
					ret = ret.replace("</title>","");
					ret = ret.trim();
				}
				//ret = "titleatitle";
			} else if (pass == 1) {
				if (!rHtml.test(tmpHtmlStr)) {
					res.destroy();
					lastPass = true;
				}
			} else {
				if (!lastPass && tmpHtmlStr.length > maxSize) {
					res.destroy();
					lastPass = true;
				}
			}
			 /*else if (pass == 2) {
				if (!rTitle.test(tmpHtmlStr)) {
					res.destroy();
					lastPass = true;
				}
			}*/

			pass++;

			if (titleFound || lastPass) {
				cb(ret);
			}
		});
	});

	return ret;
}

app.post("/todo/apiGetTitleFromUrl", (req, res) => {
	var url = req.body.url;
	
	//url = "https://www.heise.de";

	GetTitleFromUrl(url, false, (title) => {
		var json ="";

		if (title.length == 0) {
			GetTitleFromUrl(url, true, (title2) => {
				json = JSON.stringify(title2);
				res.status(200).send(json);
			});
		} else {
			json = JSON.stringify(title);
			res.status(200).send(json);
		}
	});
});
/*
function getList(tagBase, tagSearch, res) {
	tagGetItems((tags) => {
		todoGetItems(tags, tagBase, tagSearch, (todoItems, tags) => {
			res.render('todolist', { todoitems: todoItems, tags: tags  })
		})
	})
}*/
/*
app.get('/todo', (req, res) => {
	var searchTagId = 0;

	if (req.query['searchTag']) {
		searchTagId = parseInt(req.query['searchTag']);
	}

	tagGetItems((tags) => {
		todoGetItems(tags, (todoItems, tags) => {
			res.render('todolist', { todoitems: todoItems, tags: tags  })
		})
	})

})
*/


/*
app.get('/APIaddItem', (req, res) => {
	var id = null;

	if (req.query['id']) {
		id = parseInt(req.query['id']);
		if (id == 0) {
			id = null;
		}

	}

	var done = 0;
	if (req.query['done']) {
		done = req.query['done'];
	}

	var name = "";
	if (req.query['name']) {
		name = req.query['name'];

		todoAddOrUpdate(id, done, name);

		//return res.end();
	}
	
});

app.get('/addItem', (req, res) => {
	var tags = [];

	if (req.query['title']) {
		var title = req.query['title'];
		var sp = title.split('#');
		if (sp.length > 0) {
			title = sp[0];
			for (var i = 1; i < sp.length; i++) {
				tags.push(sp[i]);
			}
		}

		todoAddOrUpdate(null, 0, title, tags);
	}

	return res.redirect('./todo');
})



app.get('/delItem', (req, res) => {
	var id = null;

	if (req.query['id']) {
		id = parseInt(req.query['id']);
		if (id == 0) {
			id = null;
		}

	}

	if (id) {
		todoDel(id);
	}
	
	return res.redirect('./todo');
});*/

app.get('/tagAdd', (req, res) => {
	if (req.query['title']) {
		var title = req.query['title'];
		tagAddOrUpdate(null, 0, 0, "", title);
	}

	return res.redirect('./todo');
})

app.get('/tagDel', (req, res) => {
	var id = null;

	if (req.query['id']) {
		id = parseInt(req.query['id']);
		if (id == 0) {
			id = null;
		}
	}

	if (id) {
		tagDel(id);
	}
	
	return res.redirect('./todo');
});

app.listen(port, () => {
	console.log(`Server started at port ${port}`);
	db = new sqlite.Database(config.dbPath, (err) => {
		if (err) {
		  return console.error(err.message);
		}
		console.log('Connected to the SQlite database.');
		userGetAll();
	  });
});

function tagExists(tagName, cb) {
	var ret = undefined;

	if (allTags.length == 0) {
		tagGetItems((tags) => {
			ret = tagExistsBase(tagName);
			if (cb) {
				cb(ret);
			}
		});
	} else {
		ret = tagExistsBase(tagName);
		if (cb) {
			cb(ret);
		}
	}
}

function tagExistsBase(tagName) {
	var ret = undefined;

	for (var key in allTags) {
		var tag = allTags[key];

		if (tag.name == tagName) {
			ret = tag.id;
			break;
		}
	}

	return ret;
}

function tag2TodoExists(todoId, tagId, cb) {
	var ret = -1;

	var sql = `SELECT ${getSQLValue("id", SQLTYPE.UUID)}, ${getSQLValue("idtodo", SQLTYPE.UUID)} from Tag2Todo where idtag = ${setSQLValue(tagId, SQLTYPE.UUID)} AND idtodo =  ${setSQLValue(todoId, SQLTYPE.UUID)}`;
	
	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			if (rows && rows.length > 0) {
				ret = rows[0].Id;
			}

			if (cb) {
				cb(ret)
			}
 		}
	});
	
}


function tag2TodoAddDb(todoId, tagId) {
	tag2TodoExists(todoId, tagId, (tag2TodoId) => {
		if (tag2TodoId == -1) {
			var sql =`INSERT INTO Tag2Todo(id, idtag, idtodo)\
			VALUES(${setSQLValue(uuidCreate(), SQLTYPE.UUID)}, ${setSQLValue(tagId, SQLTYPE.UUID)}, ${setSQLValue(todoId, SQLTYPE.UUID)})`;
		
			db.run(sql, (err) => {
				if (err) {
					console.error(err.message);
				}
			});
		}
	});
	
}

function tag2TodoAdd(todoId, tagName) {
	tagExists(tagName, (tagId) => {
		if (tagId) {
			tag2TodoAddDb(todoId, tagId);
		} else {
			tagAddOrUpdate(null, 0, 0, "", tagName, (tagId) => {
				if (tagId) {
					tag2TodoAddDb(todoId, tagId);
				}
			});
		}
	});
}

function tag2TodoDel(todoId, tagId) {
	tag2TodoExists(todoId, tagId, (tag2TodoId) => {
		var sql = `DELETE FROM tag2todo WHERE id = ${tag2TodoId}`;
		
		db.run(sql, (err) => {
			if (err) {
				console.error(err.message);
			} else {
			}
		});
	});
}

function tag2TodoGetItems(cb) {
	var sql = `SELECT 	${getSQLValue("id", SQLTYPE.UUID, "t")} as id,\
						${getSQLValue("idtag", SQLTYPE.UUID, "t")} as idtag,\
						${getSQLValue("idtodo", SQLTYPE.UUID, "t")} as idtodo \
				FROM tag2Todo t`;

	/*if (itemId) {
		sql += ' INNER JOIN Tag2Todo t2t on t2t.IdTag = t.id';
		sql += ' WHERE t2t.IdTodo = '+itemId;
	}*/

	//sql += ' WHERE t.iduser = '+ userId;

	//sql += " ORDER BY name";

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			//allTags = rows;
			if (cb) {
				cb(rows);
			}
		}
	});
}

function noteGetItemsFor(id, cb) {
	return noteGetItemsBase(null, id, cb);
}

function noteGetItemsForP(id, cb) {
	return new Promise((resolve, reject) => {
		return noteGetItemsBase(null, id, (notes) => {
			if (cb) {
				cb(notes);
			}

			resolve();
		});
	});
}

function todoGetItemP(id, cb) {
	return new Promise((resolve, reject) => {
		return todoGetItem(id, (item) => {
			if (cb) {
				cb(item);
			}

			resolve();
		});
	});
}


function noteGetItem(id, cb) {
	return noteGetItemsBase(id, null, (rows) => {
		var ret = null;
		if (rows && rows.length > 0) {
			ret = rows[0];
		}

		if (cb) {
			cb(ret);
		}
	});
	/*var sql = `SELECT 	${getSQLValue("id", SQLTYPE.UUID, "t")} as id,`
	sql +=`${getSQLValue("idfk", SQLTYPE.UUID, "t")} as idfk,`
	sql +=`${getSQLValue("note", SQLTYPE.TEXT, "t")} as note,`
	sql +=`${getSQLValue("notehtml", SQLTYPE.TEXT, "t")} as notehtml,`
	sql +=`${getSQLValue("idusercreated", SQLTYPE.TEXT, "t")} as idusercreated,`
	sql +=`${getSQLValue("datecreated", SQLTYPE.TEXT, "t")} as datecreated,`
	sql +=`${getSQLValue("idusermodified", SQLTYPE.TEXT, "t")} as idusermodified,`
	sql +=`${getSQLValue("datemodified", SQLTYPE.TEXT, "t")} as datemodified`
	sql +=` FROM note t WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;


	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				cb(rows[0]);
			}
		}
	});*/
}

function noteGetItemsBase(id, idfk, cb) {
	var sql = `SELECT 	${getSQLValue("id", SQLTYPE.UUID, "t")} as id,`
	sql +=`${getSQLValue("idfk", SQLTYPE.UUID, "t")} as idfk,`
	sql +=`${getSQLValue("idtodo", SQLTYPE.UUID, "t")} as idtodo,`
	sql +=`${getSQLValue("idjournal", SQLTYPE.UUID, "t")} as idjournal,`
	sql +=`${getSQLValue("idkb", SQLTYPE.UUID, "t")} as idkb,`
	sql +=`${getSQLValue("idtask", SQLTYPE.UUID, "t")} as idtask,`
	sql +=`${getSQLValue("status", SQLTYPE.NUMBER, "t")} as status,`
	sql +=`${getSQLValue("note", SQLTYPE.TEXT, "t")} as note,`
	sql +=`${getSQLValue("notehtml", SQLTYPE.TEXT, "t")} as notehtml,`
	sql +=`${getSQLValue("idusercreated", SQLTYPE.UUID, "t")} as idusercreated,`
	sql +=`${getSQLValue("datecreated", SQLTYPE.NUMBER, "t")} as datecreated,`
	sql +=`${getSQLValue("idusermodified", SQLTYPE.UUID, "t")} as idusermodified,`
	sql +=`${getSQLValue("datemodified", SQLTYPE.NUMBER, "t")} as datemodified`
	
	if (idfk) {
		sql +=` FROM note t WHERE idfk = ${setSQLValue(idfk, SQLTYPE.UUID)}`;
	} else {
		sql +=` FROM note t WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
	}

	sql += " ORDER BY datecreated DESC";

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message +sql);
		} else {
			if (cb) {
				cb(rows);
			}
		}
	});
}

var SQLTYPE = {
	NUMBER: 1,
	BOOL : 2,
	TEXT: 3,
	UUID: 4
};

var TODOSTATUS = {
	ACTIVE : 0,
	INACTIVE : 1,
	ARCHIVED : 2
};

var TODONOTESTATUS = {
	ACTIVE : 0,
	OBSOLETE: 1,
	DELETED: 2
}

var TODOTYPE = {
	TODO : 0,
	JOURNAL: 1,
	KB: 2,
	UNSORTED: 3,
	TASK:    4
}



function getSQLValueBase(column, type, tableName) {
	var ret = column;
	var prefix = "";
	
	if (tableName) {
		prefix = `${tableName}.`;
	} 

	if (column) {
		if (type == SQLTYPE.NUMBER) {
			ret = `${prefix}${column}`;
		} else if (type == SQLTYPE.UUID) {
			ret = `(CASE WHEN ${prefix}${column} IS NOT NULL THEN SUBSTR(QUOTE(${prefix}${column}), 3, 32) END)`;
		}
	}

	return ret;
}

function getSQLValueShort(column, type, tableName) {
	var ret = getSQLValueBase(column, type, tableName);
	
	if (type == SQLTYPE.UUID) {
		ret += ` AS ${column}`;
	}

	return ret;
}

function getSQLValue(column, type, tableName) {
	var ret = getSQLValueBase(column, type, tableName);
	var asColumn = "";
	var prefix = "";
	
	if (tableName) {
		prefix = `${tableName}.`;
	} else {
		asColumn = ` AS ${column}`;
	}

	ret += ` ${asColumn}`;

	return ret;
}

function setSQLValue(value, type) {
	var ret = value;

	try {
		if (value == undefined) {
			if (type == SQLTYPE.NUMBER) {
				ret = null;
			} else if (type == SQLTYPE.TEXT) {
				ret = "";
			}  else if (type == SQLTYPE.BOOL) {
				ret = false;
			} else if (type == SQLTYPE.UUID) {
				//if (typeof value == 'string') 
				{
					var uuidStr = uuid.v4();
					uuidStr = uuidStr.replace(/-/g,'');
					uuidStr =`X'${uuidStr}'`;
					ret = uuidStr;
				}
			}
		} else {
			if (value) {
				if (type == SQLTYPE.NUMBER) {
					if (value == 'null') {
						ret = 0;
					} else if (value == -1) {
						ret = null;
					}
					
				} else if (type == SQLTYPE.BOOL) {
					if (value == 'null') {
						ret = false;
					} else {
						ret = true;
					}
				} else if (type == SQLTYPE.UUID) {
					uuidStr = value.replace(/-/g,'');
					uuidStr =`X'${uuidStr}'`;
					ret = uuidStr;
				}
			} else {
				if (type == SQLTYPE.NUMBER) {
					ret = 0;
				} else if (type == SQLTYPE.TEXT) {
					ret = "";
				}  else if (type == SQLTYPE.BOOL) {
					ret = false;
				}
			}
		}
	}
	 catch (error) {
		 var i = 2;
	 }
	return ret;
}

function getIdByIdInt(tableName, idInt, type, cb) {
	var sql = `SELECT ${getSQLValue("id",SQLTYPE.UUID)}   FROM ${tableName} t`;
	sql += " WHERE t.idint = " + setSQLValue(idInt, SQLTYPE.NUMBER);
	sql += " AND t.type = " + setSQLValue(type, SQLTYPE.NUMBER);
	
	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message + sql);
		} else {
			if (rows.length > 0) {
				if (cb) { 
					cb(rows[0]);
				}
			} else {
				cb();
			}
		}
	});	
}

function getIdByIdIntP(tableName, idInt, type) {
	return new Promise((resolve, reject) => {

		getIdByIdInt(tableName, idInt, type, (id) => {
			resolve(id);
		});
	});
}
function writeCodedUrl(id, idInt, type) {
	var ret ="";

	ret = `§{${type}:${idInt}:${id}}`;

	return ret;
}

function escapeText(text) {
	var ret = text.replace(/'/g, '#24');
	return ret;
}

function replaceIdWithUrl(noteHTML, cb) {
	var ret = noteHTML;

	async function asyncRunner () {
		try {
			for (typeKey in utils.TODOTYPEDATA) {
				var typeData = utils.TODOTYPEDATA[typeKey];
				var search = typeData.PREFIX + "0";
				for (var i = 0; i < 1; i++) {
					var index = ret.indexOf(search);
					if (index != -1) {
						var idLen = typeData.IDLEN + typeData.PREFIX.length;
						var partId = ret.substr(index, idLen);
						var partIdNr = partId.substr(typeData.PREFIX.length);
						var partIntId = parseInt(partIdNr);
						var part2 = ret.substr(index + idLen);
						var part1 = ret.substr(0, index);

						var res = await getIdByIdIntP("todo", partIntId, typeData.ID);
						if (res) {
							var u = writeCodedUrl(res.id, partIntId, typeData.ID);
							ret = part1 + u + part2;
							i = -1;
							continue;
						}
						//var u = utils.getUrlForType(res.id,"muid", typeData.ID);
					
						//var what = "";
					}
				}
			}
		} catch(ex) {}

		if (cb) {
			cb(ret);
		}
	}

	asyncRunner();

	//return ret;
}

function noteUpdateText(id, noteText, noteHTML, userId, cb) {
	var date = new Date().getTime();

	if (noteText || noteHTML) {
		if (!noteHTML) {
			noteHTML = noteText;
		}

		replaceIdWithUrl(noteHTML, (noteHTMLnew) => {
			sql = `UPDATE note `;
			sql += `SET note = '${setSQLValue(noteText, SQLTYPE.TEXT)}'`;
			sql += `, notehtml = '${setSQLValue(noteHTMLnew, SQLTYPE.TEXT)}'`;
			sql += `, idusermodified = ${setSQLValue(userId, SQLTYPE.UUID)}`;
			sql += `, datemodified = '${setSQLValue(date, SQLTYPE.NUMBER)}'`;
	
			sql += ` WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
	
			db.run(sql, function(err)  {
				if (err) {
					console.error(err.message + "\n" + sql);
				} else {
					if (cb) {
						cb(id);
					}
				}
			});

		});
	} else {
		if (cb) {
			cb(-1);
		}
	}
}

function noteAddOrUpdate(idobsolete, idfk,  userId, type, status, note, noteHTML, cb) {
	var date = new Date().getTime();
	var lastId = uuidCreate();

	if (note || noteHTML) {
		if ((noteHTML && noteHTML.length == 0) || !noteHTML) {
			noteHTML = note;
		}

		noteHTML = replaceIdWithUrl(noteHTML, (noteHTMLnew) => {
			var sql =`INSERT INTO note(id, idfk, type, status, note, notehtml, idusercreated, datecreated, idusermodified, datemodified) 
			VALUES(${setSQLValue(lastId, SQLTYPE.UUID)}, ${setSQLValue(idfk, SQLTYPE.UUID)}, ${setSQLValue(type, SQLTYPE.NUMBER)}, ${setSQLValue(status, SQLTYPE.NUMBER)},'${setSQLValue(note, SQLTYPE.TEXT)}','${setSQLValue(noteHTMLnew, SQLTYPE.TEXT)}', ${setSQLValue(userId, SQLTYPE.UUID)}, ${setSQLValue(date, SQLTYPE.NUMBER)}, ${setSQLValue(userId, SQLTYPE.UUID)}, ${setSQLValue(date, SQLTYPE.NUMBER)})`;
			/*
			if (id != -1) {
				sql = `UPDATE note `;
				sql += `SET idfk = '${sqlValue(idfk, SQLTYPE.NUMBER)}'`;
	
				sql += `, type = '${sqlValue(type, SQLTYPE.TEXT)}'`;
				sql += `, status = '${sqlValue(status, SQLTYPE.NUMBER)}'`;
				sql += `, note = '${sqlValue(note, SQLTYPE.TEXT)}'`;
				sql += `, notehtml = '${sqlValue(noteHTML, SQLTYPE.TEXT)}'`;
				sql += `, idusermodified = '${sqlValue(userId, SQLTYPE.NUMBER)}'`;
				sql += `, datemodified = '${sqlValue(date, SQLTYPE.NUMBER)}'`;
	
				sql += ` WHERE id = '${id}'`;
			}*/
			
			db.run(sql, function(err)  {
				if (err) {
					console.error(err.message + "\n" + sql);
				} else {
					//lastId = this.lastID;
		
					todoUpdateNoteStatus(idobsolete, TODONOTESTATUS.OBSOLETE, () => {
						//todoUpdateNoteId(idfk, lastId, () => {
							if (cb) {
								cb(lastId);
							}
						//});
					});
				}
			});

		});

		
	} else {
		if (cb) {
			cb(-1);
		}
	}
}

function todoUpdateNoteStatus(id, status, cb) {
	if (id && id != -1) {
		sql = `UPDATE note `;

		sql += `SET status = '${setSQLValue(status, SQLTYPE.NUMBER)}' WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
		
		db.run(sql, function(err)  {
			if (err) {
				
				console.error(err.message + "\n" + sql);
			} else {
			}

			if (cb) {
				cb();
			}
		});	
	} else {
		if (cb) {
			cb();
		}
	}
}

function todoUpdateNoteId(id, noteId, type) {
	sql = `UPDATE todo `;

	sql += `SET idnote = ${setSQLValue(noteId, SQLTYPE.UUID)} WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
	
	db.run(sql, function(err)  {
		if (err) {
			
			console.error(err.message + "\n" + sql);
		} else {
		}
	});
}

function noteUpdateTodoId(id, todoId, type, cb) {
	sql = `UPDATE note `;

	if (type == TODOTYPE.TODO) {
		sql += `SET idtodo = ${setSQLValue(todoId, SQLTYPE.UUID)}`;	
	} else if (type ==TODOTYPE.KB) { 
		sql += `SET idkb = ${setSQLValue(todoId, SQLTYPE.UUID)}`;	
	} else if (type == TODOTYPE.JOURNAL) {
		sql += `SET idjournal = ${setSQLValue(todoId, SQLTYPE.UUID)}`;	
	} else if (type == TODOTYPE.TASK) {
		sql += `SET idtask = ${setSQLValue(todoId, SQLTYPE.UUID)}`;	
	}

	sql += ` WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
	
	db.run(sql, function(err)  {
		if (err) {
			
			console.error(err.message + "\n" + sql);
		} else {
			if (cb) {
				cb();
			}
		}
	});
}

function todoAddOrUpdate(id, userId, noteId, type, name, notes, url, done, priority, status, duedate, cb) {
	getMaxIdInt("todo", type, (maxId) => {
		var todoId = id;
		var date = new Date().getTime();
	
		if (!type) {
			type = TODOTYPE.TODO;
		}
	
		if (name) {
			if (utils.isUUID(id) && !utils.isUUIDEmpty(id)) {
				sql = `UPDATE todo `;
	
				sql += `SET iduser = ${setSQLValue(userId, SQLTYPE.UUID)}`;
				sql += `, idnote = ${setSQLValue(noteId, SQLTYPE.UUID)}`;
				sql += `, name = '${setSQLValue(name, SQLTYPE.TEXT)}'`;
				//sql += `, notes = '${sqlValue(notes, SQLTYPE.TEXT)}'`;
				sql += `, url = '${setSQLValue(url, SQLTYPE.TEXT)}'`;
				sql += `, priority = '${setSQLValue(priority, SQLTYPE.NUMBER)}'`;
				sql += `, done = '${setSQLValue(done, SQLTYPE.NUMBER)}'`;
				sql += `, status = '${setSQLValue(status, SQLTYPE.NUMBER)}'`;
				sql += `, type = '${setSQLValue(type, SQLTYPE.NUMBER)}'`;
				sql += `, datedue = '${setSQLValue(duedate, SQLTYPE.NUMBER)}'`;
				sql += `, idusermodified = ${setSQLValue(userId, SQLTYPE.UUID)}`;
				sql += `, datemodified = '${setSQLValue(date, SQLTYPE.NUMBER)}'`;
	
				sql += ` WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
			} else {
				todoId = uuidCreate();
				var sql =`INSERT INTO todo(id, idint, iduser, idnote, type, name, url, priority, done, status, datedue, idusercreated, datecreated, idusermodified, datemodified) 
				VALUES(${setSQLValue(todoId, SQLTYPE.UUID)}, ${setSQLValue(maxId + 1, SQLTYPE.NUMBER)},  ${setSQLValue(userId, SQLTYPE.UUID)}, ${setSQLValue(noteId, SQLTYPE.UUID)}, ${setSQLValue(type, SQLTYPE.NUMBER)},'${setSQLValue(name, SQLTYPE.TEXT)}','${setSQLValue(url, SQLTYPE.TEXT)}',${setSQLValue(priority, SQLTYPE.NUMBER)}, ${setSQLValue(done, SQLTYPE.NUMBER)}, ${setSQLValue(status, SQLTYPE.NUMBER)}, ${setSQLValue(duedate, SQLTYPE.NUMBER)}, ${setSQLValue(userId, SQLTYPE.UUID)}, ${setSQLValue(date, SQLTYPE.NUMBER)}, ${setSQLValue(userId, SQLTYPE.UUID)}, ${setSQLValue(date, SQLTYPE.NUMBER)})`;
			}
			
			 
			db.run(sql, function(err)  {
				if (err) {
					
					console.error(err.message + "\n" + sql);
				} else {
					/*
					if (todoId == -1) {
						todoId = this.lastID;
					}*/
		/*
					for (tag in tags) {
						tag2TodoAdd(todoId, tag);
						sql = 'INSERT INTO Tag2Todo(null, done, name)\
						VALUES(' + id + ',' + done+ ',"' + name +  '")';
					}*/
	
					if (cb) {
						cb(todoId);
					}
				}
			})
		} else {
			if (cb) {
				cb(-1);
			}
		}
	});
	
}

function todoDel(id) {
	if (id) {
		var sql =`DELETE FROM todo WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;

		db.run(sql, (err) => {
			if (err) {
				console.error(err.message);
			}
		})
	}
}

function todoSetStatus(id, status) {
	if (id) {
		var sql = `UPDATE todo SET status = ${status} WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;

		db.run(sql, (err) => {
			if (err) {
				console.error(err.message);
			}
		})
	}
}

function getPropertyListFromObjectArray(arr, memberName) {
	var ret = [];

	for (var i = 0; i < arr.length; i++) {
		var val = arr[i][memberName];
		ret.push(val);
	}

	return ret;
}

function getIdsListFromArr(tagIdsArr, prefix) {
	var ret = "";
	
	if (tagIdsArr) {
		for (var i = 0; i < tagIdsArr.length; i++) {
			ret += `${prefix}'${tagIdsArr[i]}',`;
		} 
		
		if (ret.length > 0) {
			ret = ret.substr(0, ret.length-1);
		}
		
	}

	return ret;
}

function getSQLLikeTerm(field, termArr, isNot) {
	var ret = "";
	var first = true;
	var notLike = "";
	if (isNot) {
		notLike = " NOT ";
	}
	
	if (termArr) {
		for (var i = 0; i < termArr.length; i++) {
			var t = termArr[i];
			if (t) {
				if (!first) {
					ret += " AND ";
				}
				ret += `${field} ${notLike} like "%${t}%"`
				first = false;
			}
		}
	}

	return ret;
}


function getSQLTodoGetItems(userId, type, searchTerms, tagBaseId, tagSearch, showDone, statusSet, todoIdsStr) {
	var sql = `SELECT ${getSQLValueShort("id", SQLTYPE.UUID, "t")},`;
	sql += `${getSQLValueShort("idint", SQLTYPE.NUMBER, "t")},`;
	sql += `${getSQLValueShort("iduser", SQLTYPE.UUID, "t")},`;
	sql += `${getSQLValueShort("idnote", SQLTYPE.UUID, "t")},`;
	sql += `${getSQLValueShort("type", SQLTYPE.NUMBER, "t")},`;
	sql += `t.name as name,`; 
	sql += `n.note as notes,`; 
	sql += `n.notehtml as notehtml,`; 
	sql += `t.url as url,`; 
	sql += `IFNULL(t.priority, 0) as priority,`; 
	sql += `IFNULL(t.done, 0) as done,`; 
	sql += `IFNULL(t.status,0) as status,`; 
	sql += `t.datedue as datedue,`; 
	sql += `${getSQLValueShort("idusercreated", SQLTYPE.UUID, "t")},`; 
	sql += `t.datecreated as datecreated,`; 
	sql += `${getSQLValueShort("idusermodified", SQLTYPE.UUID, "t")},`; 
	sql += `t.datemodified as datemodified`; 
	sql += ` FROM todo t`;

	sql += ' LEFT JOIN note n on n.id = t.idnote';
	if (tagBaseId > 0) {
		sql += "\nINNER JOIN Tag2Todo tt on t.id = tt.idtodo\n";
	}

	if (todoIdsStr && todoIdsStr.length > 0) {
		sql += ` WHERE t.id in (${todoIdsStr})`;
	} else {
		sql += " WHERE 1=1 ";
	}

	var searchSQL = getSQLLikeTerm("t.name", searchTerms, false);
	if (searchSQL && searchSQL.length > 0) {
		sql += " AND " + searchSQL;
	}


	if (tagBaseId > 0) {
		sql += " AND (tt.idtag = " + tagBaseId;

		if (tagSearch > 0) {
			sql += " OR tt.idtag = " + tagSearch;
		}

		sql += " ) ";
	}

	sql += ` AND t.iduser = ${setSQLValue(userId, SQLTYPE.UUID)}`;

	if (!showDone) {
		sql += " AND (t.done = 0 or t.done is null)";
	}

	if (!statusSet) {
		/*status = statusSet;
		sql += ` AND t.status = ${status}`;*/
		sql += ` AND (t.status = 0 or t.status is null or t.status = 'null')`;
	} else {
			
	}

	if (type) {
		sql += ` AND t.type = ${type}`;
	} else {
		sql += ` AND IFNULL(t.type, 0) == 0`;
	}
	
	if (type == TODOTYPE.JOURNAL) {
		sql += " ORDER BY IFNULL(t.datedue,0) DESC";
	} else if (type == TODOTYPE.KB) {
		sql += " ORDER BY IFNULL(t.datemodified,0) DESC";
	} else {
		sql += " ORDER BY IFNULL(t.done,0) ASC, IFNULL(t.status, 0) ASC, IFNULL(t.priority,0) DESC, t.name";
	}

	return sql;
}

function todoGetItems(userId, type, searchTerms, tags, tagBaseId, tagSearch, showDone, statusSet, cb) {
    sql = getSQLTodoGetItems(userId, type, searchTerms, tagBaseId, tagSearch, showDone, statusSet);
	
	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message + sql);
		} else {

			if (cb) {
				cb(rows, tags);
			}
		}
	});
}

function todoGetItems2(userId, type, searchTerms, idTags, idTagsExclude, showDone, statusSet, cb) {
	todoGetItemIdsForTags(idTags, idTagsExclude, (todoIds) => {
		var todoIdsArr = getPropertyListFromObjectArray(todoIds, 'id');
		var todoIdsStr = getIdsListFromArr(todoIdsArr, 'X');
		if (todoIdsStr) {
			var sql = getSQLTodoGetItems(userId, type, searchTerms, null, null, showDone, statusSet, todoIdsStr);

			db.all(sql, [], (err, rows) => {
				if (err) {
					console.error(err.message + ":" + sql );
				} else {
					if (cb) {
						cb(rows);
					}
				}
			});
		} else {
			if (cb) {
				cb([]);
			}
		}
	});
}

function todoGetItemIdsForTags(tagIds, tagIdsExclude, cb) {
	var tagIdsStr = "";
	var tagIdsExcludeStr = "";
	if (tagIds)  {
		tagIdsStr = getIdsListFromArr(tagIds, 'X');
	}

	if (tagIdsExclude) {
		tagIdsExcludeStr = getIdsListFromArr(tagIdsExclude, 'X');
	}

	var sql = `select distinct ${getSQLValue("id", SQLTYPE.UUID, "t")} as id from todo t`;
	//sql+= ` inner join tag2todo t2t on t.id = t2t.idtodo`;
	sql+= ` where `;
	
	if (tagIds) {
		var subSelect =`select idtodo from Tag2Todo t2t where t2t.idtag in (${tagIdsStr})`;
		sql+= ` t.id in (${subSelect})`;
	}

	if (tagIds && tagIdsExclude) {
		sql += ` and`; 
	}
	
	if (tagIdsExclude) {
		var subSelect =`select idtodo from Tag2Todo t2t where t2t.idtag in (${tagIdsExcludeStr})`;
		sql+= ` t.id not in (${subSelect})`;

		//sql += ` t2t.idtag not in (${tagIdsExcludeStr})`; 
	}

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				cb(rows);
			}
		}
	});
}

function getSQLKBSync(type, idParent) {
	var sql = `SELECT ${getSQLValueShort("id", SQLTYPE.UUID, "t")},`;
	sql += `t.md5,`; 
	sql += `t.name,`; 
	sql += `t.tagname,`; 
	sql += `t.path,`; 
	sql += `t.error,`; 
	sql += `t.datesynced`; 
	sql += ` FROM KBSync t`;

	sql += ` WHERE t.type = ${type}`;

	if (idParent) {
		sql += ` AND idparent = ${setSQLValue(idParent, TODOTYPE.UUID)}`;
	}

	return sql;
}

function kbGetSyncList(type, idParent, cb) {
	var sql = getSQLKBSync(type, idParent);

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message + ":" + sql );
		} else {
			if (cb) {
				cb(rows);
			}
		}
	});
}

function kbSyncAddOrUpdate(id, parentId, kbId, type, md5, name, tagName, path, error, datesynced, cb) {
	var sql =`INSERT INTO KbSync(id, idparent, idkb, type, md5, name, tagname, path, error, datesynced) VALUES(${setSQLValue(id, SQLTYPE.UUID)},${setSQLValue(parentId, SQLTYPE.UUID)},${setSQLValue(kbId, SQLTYPE.UUID)},${setSQLValue(type, SQLTYPE.NUMBER)},"${setSQLValue(md5, SQLTYPE.TEXT)}","${setSQLValue(name, SQLTYPE.TEXT)}","${setSQLValue(tagName, SQLTYPE.TEXT)}","${setSQLValue(path, SQLTYPE.TEXT)}","${setSQLValue(error, SQLTYPE.TEXT)}","${setSQLValue(datesynced, SQLTYPE.NUMBER)}")`;
	
	if (id > 0) {
		sql = `UPDATE Tag SET name = '${setSQLValue(name, SQLTYPE.TEXT)}',`;
		sql += `idparent = '${setSQLValue(parentId, SQLTYPE.UUID)}',`; 
		sql += `idkb = '${setSQLValue(kbId, SQLTYPE.UUID)}',`; 		
		sql += `type = '${setSQLValue(type, SQLTYPE.NUMBER)}',`; 
		sql += `md5 = '${setSQLValue(md5, SQLTYPE.TEXT)}',`; 
		sql += `name = '${setSQLValue(name, SQLTYPE.TEXT)}'`; 
		sql += `tagname = '${setSQLValue(tagName, SQLTYPE.TEXT)}'`; 
		sql += `path = '${setSQLValue(path, SQLTYPE.TEXT)}'`; 
		sql += `error = '${setSQLValue(error, SQLTYPE.TEXT)}'`; 
		sql += `datesynced = '${setSQLValue(datesynced, SQLTYPE.TEXT)}'`; 								
		sql += `WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
	}
	
	db.run(sql, (err) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				cb(tagId);
			}
		}
	})
}

function kbSyncItems() {
	
	//kbSyncAddOrUpdate(null, null, 1, "", "", "", "", "", 0);

	kbGetSyncList(1, null, (dirItems) => {
		for (var i = 0; i < dirItems.length; i++) {
			var dir = dirItems[i];
			
			kbGetSyncList(2, dir.idparent, (fileItems) => {
				var cwd = dir.path;
				//cwd = 'D:/tmp/wlan/'
				var files = fs.readdirSync(cwd);
				if (files) {
					for (var j = 0; j < files.length; j++) {
						var file = files[j];
						var filePath = path.join(cwd, file)
						var buf = fs.readFileSync(filePath);
						var hash = md5(buf);
						if (hash) {
							var fileItem = utils.getById(fileItems, hash, "md5");
							if (!fileItem) {
								fileItem = utils.getById(fileItems, filePath, "path");
								if (fileItem) {
									//update item
									createKBFromDocx(filePath, update, (kbItem) => {
										kbSyncAddOrUpdate(fileItem.id, fileItem.idparent, 2, hash, fileItem.name, fileItem.tagname, fileItem.path, "", 0);
									});

								} else {
									//new item
									createKBFromDocx(filePath, (kbItem) => {
										kbSyncAddOrUpdate(null, dir.id, 2, hash, file, file, filePath, "", 0);
									});
									
									
								}
							}
							var c = 0;
						}
					}
				}	
			});
		}
		
	});
}

function tagAddOrUpdate(id, parentId, priority, color, name, cb) {
	var sql =`INSERT INTO Tag(id, parentid, priority, color, name) VALUES(${setSQLValue(id, SQLTYPE.UUID)},${setSQLValue(parentId, SQLTYPE.NUMBER)},${setSQLValue(priority, SQLTYPE.NUMBER)},"${setSQLValue(color, SQLTYPE.TEXT)}","${setSQLValue(name, SQLTYPE.TEXT)}")`;
	
	if (id > 0) {
		sql = `UPDATE Tag SET name = '${setSQLValue(name, SQLTYPE.TEXT)}', parentId = '${setSQLValue(parentId, SQLTYPE.NUMBER)}', priority = '${setSQLValue(priority, SQLTYPE.NUMBER)}', color = '${setSQLValue(color, SQLTYPE.TEXT)}', name = '${setSQLValue(name, SQLTYPE.TEXT)}' WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
	}
	
	db.run(sql, (err) => {
		if (err) {
			console.error(err.message);
		} else {
			tagGetItems((tags) => {
				tagExists(name, (tagId) => {
					if (cb) {
						cb(tagId);
					}
				});
			});
		}
	})
}

function tagDel(id) {
	if (id) {
		var sql ='DELETE FROM tag WHERE id = ' + id;

		db.run(sql, (err) => {
			if (err) {
				console.error(err.message);
			}
		})
	}
}

function coinAddInvestmentGroup(userId, name, cb) {
	var date = new Date().getTime();
	var sql ='INSERT INTO CoinInvestmentGroup(id, iduser, name, datecreated)';
	sql+= ` VALUES(null,'${userId}','${name}','${date}')`;

	db.run(sql, (err) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				cb();
			}
		}
	});
}

function coinGroupClearLastUsed(userId, cb) {
	var sql =  `UPDATE CoinInvestmentGroup set lastused = 0 WHERE iduser = ${setSQLValue(userId, SQLTYPE.UUID)} `;

	db.run(sql, (err) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				cb();
			}
		}
	});
}

function coinGroupSetLastUsed(userId, groupIdSelected, cb) {
	coinGroupClearLastUsed(userId, () => {
		var sql = `UPDATE CoinInvestmentGroup set lastused = 1 WHERE id =  ${setSQLValue(groupIdSelected, SQLTYPE.NUMBER)}  `;
	
		db.run(sql, (err) => {
			if (err) {
				console.error(err.message);
			} else {
				if (cb) {
					cb();
				}
			}
		});
	});

}

function coinGetGroups(userId, cb) {
	var sql = `SELECT g.id, g.name, g.datecreated FROM CoinInvestmentGroup g`;
	sql += ` WHERE g.iduser = ${setSQLValue(userId, SQLTYPE.UUID)}`;
	//sql += ` WHERE g.iduser = ${setSQLValue(userId, SQLTYPE.NUMBER)}`;
	sql += ` ORDER BY g.lastused DESC, g.datecreated DESC`;


	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				cb(rows);
			}
		}
	});
};

function coinGetItems(userId, groupId, cb) {
	var sql = `SELECT c.name, ci.amount, ci.valueinitial, ci.valueinitialEUR, (SELECT rate FROM CoinRate WHERE idcoin = ci.idcoin ORDER BY date DESC LIMIT 1 ) as rate, (SELECT date FROM CoinRate WHERE idcoin = ci.idcoin ORDER BY date DESC LIMIT 1 ) as date FROM CoinInvestment ci`;
	sql += ` LEFT JOIN Coin c on c.id = ci.idcoin`;
	sql += ` WHERE ci.iduser = ${setSQLValue(userId, SQLTYPE.UUID)}`;
	if (groupId > 0) {
		sql += ` AND ci.idgroup = '${groupId}'`;
	}
	sql += ` ORDER BY ci.idcoin`;


	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message + sql);
		} else {
			if (cb) {
				cb(rows);
			}
		}
	});
};

function coinGetRates(userId, cb) {
	var sql = `SELECT cr.name, ci.amount, ci.valueinitial, ci.valueinitialEUR, (SELECT rate FROM CoinRate WHERE idcoin = ci.idcoin ORDER BY date DESC LIMIT 1 ) as rate, (SELECT date FROM CoinRate WHERE idcoin = ci.idcoin ORDER BY date DESC LIMIT 1 ) as date FROM CoinInvestment ci`;
	sql += ` LEFT JOIN Coin c on c.id = ci.idcoin`;
	sql += ` WHERE ci.iduser = '${userId}'`;
	sql += ` ORDER BY ci.idcoin`;


	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				cb(rows);
			}
		}
	});
};

function coinInsertRate(coinId, rate, date, cb) {
	var sql ='INSERT INTO CoinRate(id, idcoin, rate, date)';
	sql+= ` VALUES(null,'${coinId}','${rate}','${date}')`;

	db.run(sql, (err) => {
		if (err) {
			console.error(err.message);
		} else {
			if (cb) {
				cb();
			}
		}
	});
}



function uuidCreate() {
	return uuid.v4();
}

function uuidCreateEmpty() {
	var ret = "";
	ret = "0".repeat(32);
	return ret;
}

function uuidToShortUuid(uuidStr) {
	var ret = "";
	
	if (uuidStr) {
		ret  = uuidStr.replace(/-/g,'');
	}
	
	return ret;
}


function uuidToStr() {

}

function getMaxIdInt(tableName, type, cb) {
	var sql = `SELECT MAX(idint) as maxid FROM ${tableName} `;
	sql += ` WHERE type = ${type}`;
	
	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message + sql);
		} else {
			if (cb) {
				if (rows[0]) {
					cb(rows[0].maxid);
				} else {
					cb(-1);
				}
			}
 		}
	});
}

function updateId(tableName, existingIdColName, existingIdValue, newIdColName, newIdValue, cb) { 
	if (existingIdValue != -1) {
		if (!newIdColName) {
			newIdColName = 'uid';
		}

		var uuidStr = uuid.v4();
		if (newIdValue) {
			uuidStr = newIdValue;
		}
		//uuidStr = uuidStr.replace(/-/g,'');
		//uuidStr =`X'${uuidStr}'`;
		sql = `UPDATE ${tableName} `;
		//sql += `SET gid = ${uuidStr}`;
		sql += `SET ${newIdColName} = ${setSQLValue(uuidStr, SQLTYPE.UUID)} `
		sql += ` WHERE ${existingIdColName} = '${existingIdValue}'`;
	

		db.run(sql, function(err)  {
			if (err) {
				console.error(err.message + "\n" + sql);
			} else {
				lastId = this.lastID;
			}

			if (cb) {
				cb();
			}
		});
	}
}

function updateIdP(tableName, existingIdColName, existingIdValue, newIdColName, newIdValue) {
	return new Promise((resolve, reject) => {

		updateId(tableName, existingIdColName, existingIdValue, newIdColName, newIdValue, () => {
			resolve();
		});
	});
}



function updateIdForTable(tableName, existingIdColName, newIdColName) {
	var sql = `SELECT ${getSQLValue(existingIdColName, SQLTYPE.NUMBER)} FROM ${tableName} `;
	
	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message + sql);
		} else {
			async function asyncRunner () {
				try {
					for (var i = 0; i < rows.length; i++ ) {
						//if (!rows[i].uid)
						await updateIdP(tableName, existingIdColName, rows[i][existingIdColName], newIdColName);
						console.log("updateIdForTable:" + i + ", " + rows[i][existingIdColName]);
					}
				} catch (error) {
					console.error(error)
				}
			};

			asyncRunner();
		}
	});
}




function updateIdInt(tableName, id, newIdValue, cb) { 
	if (id != -1) {
		 
			newIdColName = 'idint';
		
  
		sql = `UPDATE ${tableName} `;
		//sql += `SET gid = ${uuidStr}`;
		sql += `SET ${newIdColName} = ${setSQLValue(newIdValue, SQLTYPE.NUMBER)} `
		sql += ` WHERE id = ${setSQLValue(id, SQLTYPE.UUID)}`;
	

		db.run(sql, function(err)  {
			if (err) {
				console.error(err.message + "\n" + sql);
			} else {
				//lastId = this.lastID;
			}

			if (cb) {
				cb();
			}
		});
	}
}

function updateIdIntP(tableName, id, newIdValue) {
	return new Promise((resolve, reject) => {

		updateIdInt(tableName, id, newIdValue, () => {
			resolve();
		});
	});
}

function updateIdIntForTable(tableName, type) {
	getMaxIdInt(tableName, type, (maxId) => {
		if (maxId == null) {
			maxId = 0;
		} else if (maxId == -1) {
			return;
		}

		var sql = `SELECT ${getSQLValue("id", SQLTYPE.UUID)}, ${getSQLValue("idint", SQLTYPE.NUMBER)} FROM ${tableName} `;
		if (type != -1) {
			sql += ` WHERE type = ${type} AND idint IS NULL`;
		}
		sql += ` ORDER BY datecreated ASC`;
	
		db.all(sql, [], (err, rows) => {
			if (err) {
				console.error(err.message + sql);
			} else {
				async function asyncRunner () {
					try {
						for (var i = 0; i < rows.length; i++ ) {
							//if (!rows[i].uid)
							//await updateIdP(tableName, rows[i].id, newIdColName);
							if (rows[i].idint == null) { 
								await updateIdIntP(tableName, rows[i].id, ++maxId);
								console.log("updateIdForTable:" + i + ", " + rows[i].id + "," + maxId);
							}
						}
					} catch (error) {
						console.error(error)
					}
				};
	
				asyncRunner();
			}
		});
	});


}

function updateUserIdForTable(tableName, oldIdColName, newIdColName) {
	var sql = `SELECT ${getSQLValue("id", SQLTYPE.NUMBER)}, ${oldIdColName} FROM ${tableName} `;
	
	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message + sql);
		} else {
			async function asyncRunner () {
				try {
					for (var i = 0; i < rows.length; i++ ) {
						var intUserId = rows[i][oldIdColName];
						if (intUserId != null) {
							var user = userGetById(intUserId);
							await updateIdP(tableName, rows[i].id, newIdColName, user.uid);
							console.log("updateIdForTable:" + i + ", " +user.id, ", " +user.uid);
						}
					}
				} catch (error) {
					console.error(error)
				}
			};

			asyncRunner();
		}
	});
}

function todoGetItem(todoId, cb) {
	var sql = `SELECT ${getSQLValue("id",SQLTYPE.UUID)}, ${getSQLValue("idint",SQLTYPE.NUMBER)}, ${getSQLValue("idnote",SQLTYPE.UUID)}, t.name as name, t.notes as notes, t.url as url, t.type as type, t.done as done, t.status as status, t.priority as priority, t.datedue as datedue, t.idusercreated as idusercreated, t.datecreated as datecreated, t.idusermodified as idusermodified, t.datemodified as datemodified  FROM todo t`;

	if (utils.isUUID(todoId)) {
		sql += " WHERE t.id = " + setSQLValue(todoId, SQLTYPE.UUID);
	} else {
		sql += " WHERE t.idint = " + setSQLValue(todoId, SQLTYPE.NUMBER);
	}
	

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message + sql);
		} else {
			if (rows.length > 0) {
				if (!rows[0].id) {
					
					var d = ";"
					//updateId("todo", rows[0].id);
				}
				
				if (cb) { 
					cb(rows[0]);
				}
			} else {
				cb();
			}
		}
	});


}

function journalGetItem(journalId, journalDate, cb) {
	var sql = `SELECT ${getSQLValue("id",SQLTYPE.UUID,"t")} as id, t.name as name, t.notes as notes, t.url as url, t.done as done, t.status as status, t.priority as priority, t.datedue as datedue, t.idusercreated as idusercreated, t.datecreated as datecreated, t.idusermodified as idusermodified, t.datemodified as datemodified  FROM todo t`;

	if (utils.isUUID(journalId)) {
		sql += " WHERE t.id = " + setSQLValue(journalId, SQLTYPE.UUID);
	} else {
		sql += " WHERE t.datedue = " + setSQLValue(journalDate, SQLTYPE.NUMBER);
	}

	sql += " AND t.type = " + setSQLValue(TODOTYPE.JOURNAL, SQLTYPE.NUMBER);
	
	db.all(sql, [], (err, rows) => {
		var ret = null;

		if (err) {
			console.error(err.message + sql);
		} else {
			
			if (rows.length > 0) {
				ret = rows[0]; 
			} 
		}

		if (cb) {
			cb(ret);
		}
	});
}

function tagGetItems(cb) {
	tagGetItemsForTodoItem(null, cb);
}

function tagGetItemsForTodoItem(itemId, cb) {
	var sql = `SELECT 	${getSQLValue("id", SQLTYPE.UUID, "t")} as id,\
						${getSQLValue("parentid", SQLTYPE.UUID, "t")} as parentid,\
					t.name as name \
					FROM tag t`;

	if (itemId) {
		sql += ' INNER JOIN Tag2Todo t2t on t2t.idtag = t.id';
		if (utils.isUUID(itemId)) {
			sql += ' WHERE t2t.idtodo = '+ setSQLValue(itemId, SQLTYPE.UUID);
		} else {
			sql += ' WHERE t2t.IdTodo = '+ setSQLValue(itemId, SQLTYPE.NUMBER);
		}
	}

	sql += " ORDER BY name";

	db.all(sql, [], (err, rows) => {
		if (err) {
			console.error(err.message +":" + sql);
		} else {
			allTags = rows;
			if (cb) {
				cb(rows);
			}
		}
	});
}

function taskGroupGetItems(cb) {
	var sql = `SELECT 	${getSQLValue("id", SQLTYPE.UUID, "t")} as id,`;
	sql += `${getSQLValue("idint", SQLTYPE.NUMBER, "t")} as idint,`;
	sql += `${getSQLValue("name", SQLTYPE.TEXT, "t")} as name,`;
	sql += `${getSQLValue("status", SQLTYPE.NUMBER, "t")}  as status,`;
	sql += `${getSQLValue("priority", SQLTYPE.NUMBER, "t" )}  as priority,`;
	sql += `${getSQLValue("idusercreated", SQLTYPE.UUID, "t")} as idusercreated,`;
	sql += `${getSQLValue("datecreated", SQLTYPE.NUMBER, "t")} as datecreated,`;
	sql += `${getSQLValue("idusermodified", SQLTYPE.UUID, "t")} as idusermodified,`;
	sql += `${getSQLValue("datemodified", SQLTYPE.NUMBER, "t")} as datemodified`;
	sql += ` FROM taskGroup t`;

	sql += " ORDER BY name";

	db.all(sql, [], (err, rows) => {
		//updateId("TaskGroup", 1, 'id');
		//updateIdForTable("TaskGroup", "idint", "id");
		if (err) {
			console.error(err.message +":" + sql);
		} else {
			allTags = rows;
			if (cb) {
				cb(rows);
			}
		}
	});
}

/*

const express = require('express');
const app = express();
const port = 3000;

app.use((req, res, next) => {
	console.log(`URL: ${req.url}`);
	next();
});

app.get('/', (req, res) => {
	res.status(200).sendFile(__dirname +  '/index.html');
});

app.get('/member/:name/planet/:home', (req, res) => {
	res.end(`Requested member with name: ${req.params.name}
			from planet: ${req.params.home}`);
});

app.get('*', (req, res, next) => {
	res.status(200).send('Sorry, page not found');
	next();
});

app.listen(port, () => {
	console.log(`Server started at port ${port}`);
});
*/

/*
const rp = require('request-promise');

const requestOptions = {
  method: 'GET',
  uri: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',
  qs: {
    'start': '1',
    'limit': '5000',
    'convert': 'USD'
  },
  headers: {
    'X-CMC_PRO_API_KEY': 'b54bcf4d-1bca-4e8e-9a24-22ff2c3d462c'
  },
  json: true,
  gzip: true
};

rp(requestOptions).then(response => {
  console.log('API call response:', response);
}).catch((err) => {
  console.log('API call error:', err.message);
});

var http = require('http');

*/

function httpGetCoinRate(cb) {
	//cb(null);
	//return;
	//debug
	var debug = false;

	var token = 'xxx';
	var apiHost = 'pro-api.coinmarketcap.com';
	
	if (debug) {
		token = 'b54bcf4d-1bca-4e8e-9a24-22ff2c3d462c';
		apiHost = 'sandbox-api.coinmarketcap.com';
	}

	var apiPath ='/v1/cryptocurrency/quotes/latest?symbol=BTC,ETH,DOGE';

	var options = {
		host: apiHost,
		path: apiPath,
		port: '443',
		headers: {'X-CMC_PRO_API_KEY': token }
	};

	var req = https.request(options, (response) => {;
		var str = ''
		
		response.on('data', function (chunk) {
			str += chunk;
		});
	
		response.on('end', function () {
			cb(JSON.parse(str));
		});
	}).on('error', function(err){
		console.log(err);
		cb(null);
	});

	req.end();
}

//setImmediate(
setInterval(
	() => {
		return;
	console.log(`Check for current rates:`);
	
	httpGetCoinRate( (rates) => {
		if (rates && rates.data) {
			var btc = rates.data.BTC;
			var btcPrice = btc.quote.USD.price;

			var eth = rates.data.ETH;
			var ethPrice = eth.quote.USD.price;
	
			
			var doge = rates.data.DOGE;
			var dogePrice = doge.quote.USD.price;

			var date = new Date(); 
			var d = date.getTime();

			console.log(`${date.toDateString()} ${date.toTimeString().substr(0, 8)}: BTC: ${btcPrice}, ETH: ${ethPrice}, DOGE: ${dogePrice}`);

			coinInsertRate(1, btcPrice, d, () => {
				coinInsertRate(2, ethPrice, d, () => {
					coinInsertRate(4, dogePrice, d, () => {
					});
				});
			});
		} else {
			console.error(`${new Date().toDateString()} ${new Date().toTimeString().substr(0, 8)}: Couldn't reach coinmarketcap API`);
		}
	});
}
, 1000*60*120 //2 //120 
);

function cloneObject(obj) {
    // Handle the 3 simple types, and null or undefined
    if (null == obj || "object" != typeof obj) return obj;

    // Handle Date
    if (obj instanceof Date) {
        var copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    // Handle Array
    if (obj instanceof Array) {
        var copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = cloneObject(obj[i]);
        }
        return copy;
    }

    // Handle Object
    if (obj instanceof Object) {
        var copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = cloneObject(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

